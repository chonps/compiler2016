import ir.IrBuilder;
import ir.MIPSBuilder;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import parser.MircnosLexer;
import parser.MircnosParser;
import symboltable.SymbolTable;
import visitor.Ichi;
import visitor.Ni;
import visitor.San;

import java.io.*;

public class Main {

    static SymbolTable symbolTable;

    private static void phase1(InputStream src) throws IOException {
        InputStreamReader Src = new InputStreamReader(src);
        ANTLRInputStream input = new ANTLRInputStream(Src);
        MircnosLexer lexer = new MircnosLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        MircnosParser parser = new MircnosParser(tokens);

        ParseTree tree = parser.compilationUnit();
        if (parser.getNumberOfSyntaxErrors() > 0) {
            throw new RuntimeException("Syntax Error");
        }

        Ichi ichi = new Ichi();
        ichi.symbolTable = symbolTable;
        ichi.visit(tree);

		Ni ni = new Ni();
        ni.symbolTable = symbolTable;
        ni.visit(tree);

        San san = new San();
        san.symbolTable = symbolTable;
        san.visit(tree);

        if (symbolTable.get("main") != null) {
            if (!symbolTable.get("main").type.equals("int")) throw new RuntimeException("Not int main");
        }

        if (symbolTable.size() == 0) throw new RuntimeException("mdzz");
       /* AST_printer ast_printer = new AST_printer();
        ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(ast_printer, tree);
        */
/*
        if(symbol_table.get(Name.getSymbolName("main"), -1).funcP == null ||
                !symbol_table.get(Name.getSymbolName("main"), -1).funcP.retType.equal(symbol_table.get(Name.getSymbolName("int"), -1)))
        {
            throw new RuntimeException("main not found, or return type of main is not int");
        }
*/
        //symbol_table.print();


        try {
            SymbolTable irTable = new SymbolTable();

            Ichi Ein = new Ichi();
            Ein.symbolTable = irTable;
            Ein.visit(tree);
            Ni Zwei = new Ni();
            Zwei.symbolTable = irTable;
            Zwei.visit(tree);

            BufferedWriter fileout = new BufferedWriter(new FileWriter("ll_after.ir"));
            IrBuilder ir = new IrBuilder();
            ir.symbolTable = irTable;
            ir.fileout = fileout;
            ir.visit(tree);


/*
            ir.println("@functionLabel_print r_{str}");
            ir.println("return r_{global_return_register}");

            ir.println("@functionLabel_println r_{str}");
            ir.println("return r_{global_return_register}");

            ir.println("@functionLabel_getString");
            ir.println("return r_{global_return_register}");

            ir.println("@functionLabel_getInt");
            ir.println("return r_{global_return_register}");

            ir.println("@functionLabel_toString r_{i}");
            ir.println("loadI 0 r_{nn_*temp}\ni2i r_{i} r_{j_*temp}\nloadI 0 r_{zero_*temp}\n@Label_toString1_for:\ncmp_LT r_{zero_*temp} r_{j_*temp} r_{tmp3_*temp}\ncbr r_{tmp3_*temp} @Label_toString1_work @Label_toString1_end\n@Label_toString1_work:\naddI r_{nn_*temp} 1 r_{nn_*temp}\ndivI r_{j_*temp} 10 r_{j_*temp}\njumpI @Label_toString1_for\n@Label_toString1_end:\ncmp_LT r_{zero_*temp} r_{nn_*temp} r_{tmp3_*temp}\ncbr r_{tmp3_*temp} @Label_toString_goon @Label_toString_empty\n@Label_toString_empty:\nloadI 1 r_{nn_*temp}\n@Label_toString_goon:\ndivI r_{nn_*temp} 4 r_{sz_*temp}\naddI r_{sz_*temp} 2 r_{sz_*temp}\nmultI r_{sz_*temp} 4 r_{sz4_*temp}\nmalloc r_{sz4_*temp} r_{str}\nstore r_{nn_*temp} r_{str}\nloadI 1 r_{ii_*temp}\nloadI 1 r_{base_*temp}\n@Label_toString2_for:\ncmp_LT r_{ii_*temp} r_{nn_*temp} r_{tmp3_*temp}\ncbr r_{tmp3_*temp} @Label_toString2_work @Label_toString2_end\n@Label_toString2_work:\nmultI r_{base_*temp} 10 r_{base_*temp}\naddI r_{ii_*temp} 1 r_{ii_*temp}\njumpI @Label_toString2_for\n@Label_toString2_end:\nloadI 0 r_{ii_*temp}\nloadI 4 r_{four_*temp}\ni2i r_{i} r_{copyi_*temp}\n@Label_toString3_for:\ncmp_LT r_{ii_*temp} r_{nn_*temp} r_{tmp3_*temp}\ncbr r_{tmp3_*temp} @Label_toString3_work @Label_toString3_end\n@Label_toString3_work:\nloadI 0 r_{j_*temp}\nloadI 0 r_{ssi_*temp}\n@Label_toString4_for:\ncmp_LT r_{j_*temp} r_{four_*temp} r_{tmp3_*temp}\ncbr r_{tmp3_*temp} @Label_toString4_work @Label_toString4_end\n@Label_toString4_work:\ncmp_LT r_{zero_*temp} r_{base_*temp} r_{tmp3_*temp}\ncbr r_{tmp3_*temp} @Label_toString_then @Label_toString_else\n@Label_toString_then:\nloadI 0 r_{tmp_*temp}\ndiv r_{copyi_*temp} r_{base_*temp} r_{tmp_*temp}\ndivI r_{tmp_*temp} 10 r_{tmp2_*temp}\nmultI r_{tmp2_*temp} 10 r_{tmp2_*temp}\nsub r_{tmp_*temp} r_{tmp2_*temp} r_{tmp_*temp}\naddI r_{tmp_*temp} 48 r_{tmp_*temp}\nlshiftI r_{j_*temp} 3 r_{tmp2_*temp}\nlshift r_{tmp_*temp} r_{tmp2_*temp} r_{tmp_*temp}\nadd r_{ssi_*temp} r_{tmp_*temp} r_{ssi_*temp}\n@Label_toString_else:\ndivI r_{base_*temp} 10 r_{base_*temp}\naddI r_{j_*temp} 1 r_{j_*temp}\njumpI @Label_toString4_for\n@Label_toString4_end:\naddI r_{ii_*temp} 4 r_{ii_*temp}\nadd r_{str} r_{ii_*temp} r_{tmp_*temp}\nstore r_{ssi_*temp} r_{tmp_*temp}\njumpI @Label_toString3_for\n@Label_toString3_end:\ni2i r_{str} r_{global_return_register}");
            ir.println("return r_{global_return_register}");

            ir.println("@functionLabel___builtin__length");
            ir.println("load r_{global_builtin} r_{global_return_register}");
            ir.println("return r_{global_return_register}");

            ir.println("@functionLabel___builtin__substring r_{left} r_{right}");
            ir.println("return r_{global_return_register}");

            ir.println("@functionLabel___builtin__parseInt");
            ir.println("return r_{global_return_register}");

            ir.println("@functionLabel___builtin__ord r_{pos}");
            ir.println("return r_{global_return_register}");

            ir.println("@functionLabel___builtin__size");
            ir.println("loadAI r_{global_builtin} -4 r_{global_return_register}");
            ir.println("return r_{global_return_register}");
*/

            fileout.flush();
            fileout.close();
        } catch (Exception e) {
        }

        try{
            BufferedReader filein = new BufferedReader(new FileReader("ll_after.ir"));
            BufferedWriter fileout = new BufferedWriter(new FileWriter("mips.s"));

            MIPSBuilder mips = new MIPSBuilder();

            mips.filein = filein;
            mips.fileout = fileout;
            mips.readIt();
            mips.translate();

            fileout.flush();
            fileout.close();
            filein.close();
        } catch (Exception e){
        }

    }

    public static void main(String[] args) throws Exception { //args specified the file location
        symbolTable = new SymbolTable();
        try {
            phase1(System.in);
        } catch (Exception e) {
	        System.out.println(e.getMessage());
            System.exit(1);
        }
        System.exit(0);
    }
}
