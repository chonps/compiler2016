package ir;

import parser.MircnosBaseVisitor;
import parser.MircnosParser;
import symboltable.Declaration;
import symboltable.SymbolTable;

import java.io.BufferedWriter;
import java.util.ArrayList;

public class IrBuilder extends MircnosBaseVisitor<Declaration> {
	public SymbolTable symbolTable;
	boolean func;
	Declaration now;
	String blk;
	int regtot, lbltot;
	Integer fwl;
	int aor;
	public BufferedWriter fileout;
	ArrayList<ArrayList<String> > funcList;
	boolean vari;
	String dot;
	String fnc;
	String TMP;

	public IrBuilder () {
		funcList = new ArrayList<ArrayList<String> >();
		fwl = 0;
		aor = 0;
		fnc = "";
		TMP = "global_tmp";
	}

	@Override public Declaration visitCompilationUnit(MircnosParser.CompilationUnitContext ctx) {
		func = false;
		blk = "global";
		vari = true;
		println("@functionLabel__global");
		println("mallocI 4 r_{global_null}");
		operatI("loadI", "4", TMP);
		for (int i = 0; i < ctx.declaration().size(); ++i)
			visit(ctx.declaration(i));
		println("return r_{global_return_register}");
		vari = false;
		for (int i = 0; i < ctx.declaration().size(); ++i)
			visit(ctx.declaration(i));
//		System.out.println("???");
		return null;
	}

	@Override public Declaration visitDeclaration(MircnosParser.DeclarationContext ctx) {
		if (ctx.variableDeclarationStatement() != null && vari)
			visit(ctx.variableDeclarationStatement());
		if (ctx.functionDeclaration() != null && !vari)
			visit(ctx.functionDeclaration());
		return null;
	}

	@Override public Declaration visitFunctionDeclaration(MircnosParser.FunctionDeclarationContext ctx) {
		Declaration x = symbolTable.get(ctx.Identifier().getText());
		print("@functionLabel_" + x.name);
		symbolTable.down();
		for (int i = 0; i < x.funcList.size(); ++i) {
			Declaration y = new Declaration(x.funcList.get(i).name, x.funcList.get(i).type, 'V');
			y.register = new String(y.name + "_arg_" + i);
			print(" " + register(y.register));
			symbolTable.put(y);
		}
		println("");
		if (x.name.equals("main")) println("call @functionLabel__global");
		for (int i = 0; i < x.funcList.size(); ++i) {
			Declaration y = symbolTable.get(x.funcList.get(i).name);
			y.register = newInt();
			operat("store", y.name + "_arg_" + i, y.register);
		}
		now = x;
		blk = "function";
		visit(ctx.functionBody());
		putLabel(now.name + "_end");
		println("return r_{global_return_register}");
		blk = "global";
		symbolTable.up();
		return null;
	}

	@Override public Declaration visitFunctionBody(MircnosParser.FunctionBodyContext ctx) {
		visit(ctx.block());
		return null;
	}

	@Override public Declaration visitFunction(MircnosParser.FunctionContext ctx) {
//		???
		Declaration x = symbolTable.get(ctx.Identifier().getText());
//		System.out.println(x.name + " Start");
		if ((fnc.equals("println") || fnc.equals("print")) && x.name.equals("toString")) {
			String lst = fnc;
			fnc = x.name;
			visit(ctx.functionParametersList());
			int dep = funcList.size() - 1;
			for (int i = 0; i < funcList.get(dep).size(); ++i)
				println("call @functionLabel_printInt " + register(funcList.get(dep).get(i)));
			funcList.remove(dep);
			fnc = lst;
			return new Declaration();
		}
		String lst = fnc;
		fnc = x.name;
		if (ctx.functionParametersList() != null) {
//			System.out.println("println Parameter");
			visit(ctx.functionParametersList());
//			System.out.println("Paramater End");
		}
		if (!fnc.equals("println") && !fnc.equals("print")) {
			print("call " + "@functionLabel_" + x.register + x.name);
			if (ctx.functionParametersList() != null) {
				int dep = funcList.size() - 1;
				for (int i = 0; i < funcList.get(dep).size(); ++i)
					print(" " + register(funcList.get(dep).get(i)));
				funcList.remove(dep);
			}
			println("");
		}
		else {
			funcList.remove(funcList.size() - 1);
		}
		if (fnc.equals("println"))
			println("call @functionLabel_println");
		fnc = lst;
		if (x.type.equals("void")) return null;
		String reg = Integer.toString(++regtot);
		operat("i2i", "global_return_register", reg);
//		System.out.println("Last == " + fnc + " " + x.name);
		if (fnc.equals("print") || fnc.equals("println")) {
			printString(reg);
		}
		return new Declaration("", x.type, 'V', reg);
	}

	@Override public Declaration visitFunctionParametersList(MircnosParser.FunctionParametersListContext ctx) {
//		???
		int dep = funcList.size();
		funcList.add(new ArrayList<String>());
		for (int i = 0; i < ctx.functionParameters().size(); ++i) {
			funcList.get(dep).add(visit(ctx.functionParameters(i)).register);
		}
		return null;
	}


	@Override public Declaration visitFunctionParameters(MircnosParser.FunctionParametersContext ctx) {
//		???
		Declaration x = new Declaration();
		if (ctx.Identifier() != null) {
			x = symbolTable.get(ctx.Identifier().getText());
		}
		if (ctx.expression() != null) {
			x = visit(ctx.expression());
		}
		String reg;
		if (!x.name.equals("")) {
			reg = newReg();
			operat("load", x.register, reg);
		}
		else reg = x.register;
		if ((fnc.equals("print") || fnc.equals("println")) && ctx.Identifier() != null) {
			printString(reg);
		}
		return new Declaration("", x.type, 'V', reg);
	}

	@Override public Declaration visitType(MircnosParser.TypeContext ctx) {
		Declaration x = new Declaration();
		if (ctx.Identifier() != null) x = symbolTable.get(ctx.Identifier().getText());
		if (ctx.primitiveType() != null) x.type = visit(ctx.primitiveType()).type;
		if (ctx.type() != null) {
			x = new Declaration(visit(ctx.type()));
			x.type = new String(x.type + "[]");
		}
		return x;
	}

	@Override public Declaration visitPrimitiveType(MircnosParser.PrimitiveTypeContext ctx) {
		Declaration x = new Declaration();
		x.type = ctx.getText();
		return x;
	}

	@Override public Declaration visitLiteral(MircnosParser.LiteralContext ctx) {
		Declaration x = new Declaration();
		if (ctx.IntegerLiteral() != null) {
//			System.out.println("Integer");
			String y = ctx.IntegerLiteral().getText();
			x.register = Integer.toString(++regtot);
			operatI("loadI", y, x.register);
			x.type = "int";
		}
		if (ctx.StringLiteral() != null) {
			String y = ctx.StringLiteral().getText();
			x.register = newString(y);
			x.type = "string";
			if (fnc.equals("println") || fnc.equals("print")) {
				printString(x.register);
				return x;
			}
		}
		if (ctx.BooleanLiteral() != null) {
//			System.out.println("Boolean");
			String y;
			if (ctx.BooleanLiteral().getText().charAt(0) == 't') y = "1";
			else y = "0";
			x.register = Integer.toString(++regtot);
			operatI("loadI", y, x.register);
			x.type = "bool";
		}
		if (ctx.NULL() != null) {
			x.type = "null";
			x.register = "global_null";
		}
		return x;
	}

	@Override public Declaration visitBlock(MircnosParser.BlockContext ctx) {
		for (int i = 0; i < ctx.blockStatement().size(); ++i) {
			visit(ctx.blockStatement(i));
		}
		return null;
	}

	@Override public Declaration visitBlockStatement(MircnosParser.BlockStatementContext ctx) {
		if (ctx.variableDeclarationStatement() != null) visit(ctx.variableDeclarationStatement());
		if (ctx.statement() != null) visit(ctx.statement());
		return null;
	}

	@Override public Declaration visitVariableDeclarationStatement(MircnosParser.VariableDeclarationStatementContext ctx) {
		visit(ctx.variableDeclaration());
		return null;
	}

	@Override public Declaration visitVariableDeclaration(MircnosParser.VariableDeclarationContext ctx) {
		Declaration x = new Declaration();
		x.type = visit(ctx.type()).type;
		x.name = ctx.Identifier().getText();
		x.dectype = 'V';
		x.register = Integer.toString(++regtot) + "_variable";
		if (vari) x.register = "global_" + x.register;
		newInt(x.register);
		if (ctx.expression() != null) {
			Declaration y = visit(ctx.expression());
			if (!y.name.equals("")) {
				String tmp = TMP;
				operat("load", y.register, tmp);
				operat("store", tmp, x.register);
			}
			else operat("store", y.register, x.register);
		}
		symbolTable.put(x);
		return null;
	}

	@Override public Declaration visitBlockStmt(MircnosParser.BlockStmtContext ctx) {
		symbolTable.down();
		visit(ctx.block());
		symbolTable.up();
		return null;
	}

	@Override public Declaration visitIf(MircnosParser.IfContext ctx) {
		Declaration x = visit(ctx.parExpression());
		String reg;
		if (x.name.length() > 0) {
			reg = newRegTemp();
			operat("load", x.register, reg);
		}
		else reg = x.register;
		String ifl = "_if_" + Integer.toString(++lbltot);
		cbr(reg, ifl + "_then", ifl + "_else");
		putLabel(ifl + "_then");
		visit(ctx.statement(0));
		jumpI(ifl + "_end");
		putLabel(ifl + "_else");
		if (ctx.statement().size() > 1) visit(ctx.statement(1));
		putLabel(ifl + "_end");
		return null;
	}

	@Override public Declaration visitFor(MircnosParser.ForContext ctx) {
		Integer lst = new Integer(fwl);
		String bst = blk;
		blk = "for";
		fwl = ++lbltot;
		symbolTable.down();
		visit(ctx.forControl());
		putLabel(fwl.toString() + "_work");
		visit(ctx.statement());
		jumpI(fwl.toString() + "_update");
		putLabel(fwl.toString() + "_end");
		fwl = lst;
		blk = bst;
		symbolTable.up();
		return null;
	}

	@Override public Declaration visitWhile(MircnosParser.WhileContext ctx) {
		Integer lst = new Integer(fwl);
		fwl = ++lbltot;
		String bst = blk;
		blk = "while";
		putLabel(fwl.toString() + "_while");
		Declaration x = visit(ctx.parExpression());
		String reg;
		if (x.name.length() > 0) {
			reg = newRegTemp();
			operat("load", x.register, reg);
		}
		else reg = x.register;
		cbr(reg, fwl.toString() + "_work", fwl.toString() + "_end");
		putLabel(fwl.toString() + "_work");
		visit(ctx.statement());
		jumpI(fwl.toString() + "_while");
		putLabel(fwl.toString() + "_end");
		fwl = lst;
		blk = bst;
		return null;
	}

	@Override public Declaration visitReturn(MircnosParser.ReturnContext ctx) {
//		???
		if (ctx.expression() != null) {
			Declaration x = visit(ctx.expression());
			String reg;
			if (!x.name.equals("")) {
				reg = newRegTemp();
				operat("load", x.register, reg);
			}
			else reg = x.register;
			_return(reg);
			jumpI(now.name + "_end");
			return new Declaration("", x.type, 'V', reg);
		}
		else jumpI(now.name + "_end");
		return null;
	}

	@Override public Declaration visitBreak(MircnosParser.BreakContext ctx) {
//		System.out.println("break " + fwl + " " + blk);
		jumpI(fwl.toString() + "_end");
		return null;
	}

	@Override public Declaration visitContinue(MircnosParser.ContinueContext ctx) {
		if (blk.equals("for")) jumpI(fwl.toString() + "_update");
		else jumpI(fwl.toString() + "_while");
		return null;
	}

	@Override public Declaration visitExpStmt(MircnosParser.ExpStmtContext ctx) {
		visit(ctx.statementExpression());
		return null;
	}

	@Override public Declaration visitForControl(MircnosParser.ForControlContext ctx) {
		if (ctx.forInit() != null) visit(ctx.forInit());
		putLabel(fwl.toString() + "_for");
		if (ctx.expression() != null) {
			String reg = getRegTemp(visit(ctx.expression()));
			cbr(reg, fwl.toString() + "_work", fwl.toString() + "_end");
		}
		else jumpI(fwl.toString() + "_work");
		putLabel(fwl.toString() + "_update");
		if (ctx.forUpdate() != null) visit(ctx.forUpdate());
		jumpI(fwl.toString() + "_for");
		return null;
	}

	@Override public Declaration visitForInit(MircnosParser.ForInitContext ctx) {
		if (ctx.variableDeclaration() != null) visit(ctx.variableDeclaration());
		if (ctx.expression() != null) visit(ctx.expression());
		return null;
	}

	@Override public Declaration visitForUpdate(MircnosParser.ForUpdateContext ctx) {
		if (ctx.expression() != null) visit(ctx.expression());
		return null;
	}

	@Override public Declaration visitParExpression(MircnosParser.ParExpressionContext ctx) {
		return visit(ctx.expression());
	}

	@Override public Declaration visitStatementExpression(MircnosParser.StatementExpressionContext ctx) {
		visit(ctx.expression());
		return null;
	}

	@Override public Declaration visitPrim(MircnosParser.PrimContext ctx) {
		return visit(ctx.primary());
	}

	@Override public Declaration visitNew(MircnosParser.NewContext ctx) {
		return visit(ctx.newType());
	}

	@Override public Declaration visitLogicOr(MircnosParser.LogicOrContext ctx) {
		Declaration x = visit(ctx.expression(0));
		String a;
		if (!x.name.equals("")) {
			a = Integer.toString(++regtot);
			operat("load", x.register, a);
		}
		else a = x.register;
		++aor;
		cbr(a, "or_" + aor + "_then", "or_" + aor + "_else");
		putLabel("or_" + aor + "_else");
		Declaration y = visit(ctx.expression(1));
		String b;
		if (!y.name.equals("")) {
			b = Integer.toString(++regtot) + "_*temp";
			operat("load", y.register, b);
		}
		else b = y.register;
		operator("or", a, b, a);
		putLabel("or_" + aor + "_then");
		return new Declaration("", "bool", 'V', a);
	}

	@Override public Declaration visitDotFunction(MircnosParser.DotFunctionContext ctx) {
//		???
		String lst = fnc;
		fnc = "Dot";
		Declaration x = visit(ctx.expression());
		fnc = lst;
		if (x.name.length() > 0) operat("load", x.register, "global_builtin");
		else operat("i2i", x.register, "global_builtin");
		if (x.type.equals("string")) {
			symbolTable.stringDotSet();
			Declaration y = visit(ctx.function());
			symbolTable.stringDotReset();
			return y;
		}
		if (x.type.charAt(x.type.length() - 1) == ']') {
			symbolTable.arrayDotSet();
			Declaration y = visit(ctx.function());
			symbolTable.arrayDotReset();
			return y;
		}
		return null;
	}

	@Override public Declaration visitOr(MircnosParser.OrContext ctx) {
		Declaration x = visit(ctx.expression(0)), y = visit(ctx.expression(1));
		String a;
		if (!x.name.equals("")) {
			a = Integer.toString(++regtot);
			operat("load", x.register, a);
		}
		else a = x.register;
		String b;
		if (!y.name.equals("")) {
			b = Integer.toString(++regtot) + "_*temp";
			operat("load", y.register, b);
		}
		else b = y.register;
		operator("or", a, b, a);
		return new Declaration("", "int", 'V', a);
	}

	@Override public Declaration visitFunc(MircnosParser.FuncContext ctx) {
//		???
		return visit(ctx.function());
	}

	@Override public Declaration visitDot(MircnosParser.DotContext ctx) {
		String lst = fnc;
		fnc = "Dot";
		Declaration x = visit(ctx.expression());
		fnc = lst;
		String y = ctx.Identifier().getText();
		Declaration z = new Declaration();
		z.name = y;
		z.type = symbolTable.get(x.type).typeList.get(y);
		String reg = newReg();
		operat("load", x.register, reg);
		operatorI("addI", reg, symbolTable.get(x.type).sizeList.get(y).toString(), reg);
		z.register = reg;
		if (fnc.equals("print") || fnc.equals("println")) {
			String tmp = TMP;
			operat("load", reg, tmp);
			printString(tmp);
		}
		return z;
	}

	@Override public Declaration visitDouble(MircnosParser.DoubleContext ctx) {
		Declaration x = visit(ctx.expression());
		String lst = Integer.toString(++regtot);
		operat("load", x.register, lst);
		String op;
		if (ctx.op.getType() == MircnosParser.INC) op = "addI";
		else op = "subI";
		String tmp = TMP;
		operatorI(op, lst, "1", tmp);
		operat("store", tmp, x.register);
		return new Declaration("", "int", 'V', lst);
	}

	@Override public Declaration visitAssignment(MircnosParser.AssignmentContext ctx) {
		Declaration x = visit(ctx.expression(0)), y = visit(ctx.expression(1));
		if (y.name.equals("")) operat("store", y.register, x.register);
		else {
			String tmp = TMP;
			operat("load", y.register, tmp);
			operat("store", tmp, x.register);
		}
		return x;
	}

	@Override public Declaration visitArray(MircnosParser.ArrayContext ctx) {
		Declaration x = new Declaration(visit(ctx.expression(0))), y = visit(ctx.expression(1));
		x.type = x.type.substring(0, x.type.length() - 2);
		String reg = Integer.toString(++regtot) + "_variable";
		operat("load", x.register, reg);
		String nxt = Integer.toString(++regtot) + "_*temp";
		if (y.name.equals("")) operat("i2i", y.register, nxt);
		else operat("load", y.register, nxt);
		operatorI("multI", nxt, "4", nxt);
		operator("add", reg, nxt, reg);
		x.register = reg;
		return x;
	}

	@Override public Declaration visitBitMove(MircnosParser.BitMoveContext ctx) {
		Declaration x = visit(ctx.expression(0)), y = visit(ctx.expression(1));
		String a;
		if (!x.name.equals("")) {
			a = Integer.toString(++regtot);
			operat("load", x.register, a);
		}
		else a = x.register;
		String b;
		if (!y.name.equals("")) {
			b = Integer.toString(++regtot) + "_*temp";
			operat("load", y.register, b);
		}
		else b = y.register;
		String op;
		if (ctx.op.getType() == MircnosParser.LSHIFT) op = "lshift";
		else op = "rshift";
		operator(op, a, b, a);
		return new Declaration("", "int", 'V', a);
	}

	@Override public Declaration visitMult(MircnosParser.MultContext ctx) {
		Declaration x = visit(ctx.expression(0)), y = visit(ctx.expression(1));
		String a;
		if (!x.name.equals("")) {
			a = Integer.toString(++regtot);
			operat("load", x.register, a);
		}
		else a = x.register;
		String b;
		if (!y.name.equals("")) {
			b = Integer.toString(++regtot) + "_*temp";
			operat("load", y.register, b);
		}
		else b = y.register;
		if (ctx.op.getType() != MircnosParser.MOD) {
			String op;
			if (ctx.op.getType() == MircnosParser.MUL) op = "mult";
			else op = "div";
			operator(op, a, b, a);
		}
		else {
			String tmp = TMP;
			operator("div", a, b, tmp);
			operator("mult", tmp, b, tmp);
			operator("sub", a, tmp, a);
		}
		return new Declaration("", "int", 'V', a);
	}

	@Override public Declaration visitEqual(MircnosParser.EqualContext ctx) {
		Declaration x = visit(ctx.expression(0)), y = visit(ctx.expression(1));
		if (!x.type.equals("string")) {
			String a;
			if (!x.name.equals("")) {
				a = Integer.toString(++regtot);
				operat("load", x.register, a);
			}
			else a = x.register;
			String b;
			if (!y.name.equals("")) {
				b = Integer.toString(++regtot) + "_*temp";
				operat("load", y.register, b);
			}
			else b = y.register;
			String reg = Integer.toString(++regtot);
			equal(a, b, reg);
			if (ctx.op.getType() == MircnosParser.NOTEQUAL)
				operatorI("xorI", reg, "1", reg);
			return new Declaration("", "bool", 'V', reg);
		}
		else {
			String X;
			if (!x.name.equals("")) {
				X = newRegTemp();
				operat("load", x.register, X);
			}
			else X = x.register;
			String Y;
			if (!y.name.equals("")) {
				Y = newRegTemp();
				operat("load", y.register, Y);
			}
			else Y = y.register;
			String reg = newReg();
			operatI("loadI", "0", reg);
			println("call @functionLabel_stringLess " + register(X) + " " + register(Y));
			operator("or", reg, "global_return_register", reg);
			println("call @functionLabel_stringLess " + register(Y) + " " + register(X));
			operator("or", reg, "global_return_register", reg);
			if (ctx.op.getType() == MircnosParser.EQUAL)
				operator("xorI", reg, "1", reg);
			return new Declaration("", "bool", 'V', reg);
		}
	}

	public void equal(String a, String b, String tmp) {
		operator("cmp_LT", a, b, tmp);
		String tmp2 = TMP;
		operator("cmp_LT", b, a, tmp2);
		operator("or", tmp, tmp2, tmp);
		operatorI("xorI", tmp, "1", tmp);
	}

	@Override public Declaration visitAnd(MircnosParser.AndContext ctx) {
		Declaration x = visit(ctx.expression(0)), y = visit(ctx.expression(1));
		String a;
		if (!x.name.equals("")) {
			a = newReg();
			operat("load", x.register, a);
		}
		else a = x.register;
		String b;
		if (!y.name.equals("")) {
			b = newRegTemp();
			operat("load", y.register, b);
		}
		else b = y.register;
//		++aor;
//		println("cbr " + a + " " + "@Label_and_" + aor + "then" + " " + "@Label_and_" + aor + "else");
		operator("and", a, b, a);
		return new Declaration("", "int", 'V', a);
	}

	@Override public Declaration visitXor(MircnosParser.XorContext ctx) {
		Declaration x = visit(ctx.expression(0)), y = visit(ctx.expression(1));
		String a;
		if (!x.name.equals("")) {
			a = newReg();
			operat("load", x.register, a);
		}
		else a = x.register;
		String b;
		if (!y.name.equals("")) {
			b = newRegTemp();
			operat("load", y.register, b);
		}
		else b = y.register;
		operator("xor", a, b, a);
		return new Declaration("", "int", 'V', a);
	}

	@Override public Declaration visitDotThis(MircnosParser.DotThisContext ctx) {
		return visit(ctx.expression());
	}

	@Override public Declaration visitPlus(MircnosParser.PlusContext ctx) {
		Declaration x = visit(ctx.expression(0)), y = visit(ctx.expression(1));
		if (fnc.equals("print") || fnc.equals("println")) return new Declaration();
		if (x.type.equals("int")) {
			String a;
			if (!x.name.equals("")) {
				a = newReg();
				operat("load", x.register, a);
			}
			else a = x.register;
			String b;
			if (!y.name.equals("")) {
				b = newRegTemp();
				operat("load", y.register, b);
			}
			else b = y.register;
			String op;
			if (ctx.op.getType() == MircnosParser.ADD) op = "add";
			else op = "sub";
			operator(op, a, b, a);
			return new Declaration("", "int", 'V', a);
		}
		else {
			String X;
			if (!x.name.equals("")) {
				X = newRegTemp();
				operat("load", x.register, X);
			}
			else X = x.register;
			String Y;
			if (!y.name.equals("")) {
				Y = newRegTemp();
				operat("load", y.register, Y);
			}
			else Y = y.register;
			println("call @functionLabel_stringLink " + register(X) + " " + register(Y));
			String reg = newReg();
			operat("i2i", "global_return_register", reg);
			return new Declaration("", "string", 'V', reg);
		}
	}

	@Override public Declaration visitLogicAnd(MircnosParser.LogicAndContext ctx) {
		Declaration x = visit(ctx.expression(0));
		String a;
		if (!x.name.equals("")) {
			a = newReg();
			operat("load", x.register, a);
		}
		else a = x.register;
		++aor;
		cbr(a, "and_" + aor + "_then", "and_" + aor + "_else");
		putLabel("and_" + aor + "_then");
		Declaration y = visit(ctx.expression(1));
		String b;
		if (!y.name.equals("")) {
			b = newRegTemp();
			operat("load", y.register, b);
		}
		else b = y.register;
		operator("and", a, b, a);
		putLabel("and_" + aor + "_else");
		return new Declaration("", "bool", 'V', a);
	}

	@Override public Declaration visitAnti(MircnosParser.AntiContext ctx) {
		Declaration x = visit(ctx.expression());
		String a;
		if (!x.name.equals("")) {
			a = newReg();
			operat("load", x.register, a);
		}
		else a = x.register;
		operator("xorI", a, "-1", a);
		return new Declaration("", "int", 'V', a);
	}

	@Override public Declaration visitNot(MircnosParser.NotContext ctx) {
		Declaration x = visit(ctx.expression());
		String a;
		if (!x.name.equals("")) {
			a = newReg();
			operat("load", x.register, a);
		}
		else a = x.register;
		operatorI("xorI", a, "1", a);
		return new Declaration("", "bool", 'V', a);
	}

	@Override public Declaration visitLess(MircnosParser.LessContext ctx) {
		Declaration x = visit(ctx.expression(0)), y = visit(ctx.expression(1));
		String reg;
		if (ctx.op.getType() == MircnosParser.GT || ctx.op.getType() == MircnosParser.LE) {
			Declaration k = new Declaration(x);
			x = y;
			y = k;
		}
		if (x.type.equals("int")) {
			String a;
			if (!x.name.equals("")) {
				a = newReg();
				operat("load", x.register, a);
			}
			else a = x.register;
			String b;
			if (!y.name.equals("")) {
				b = newRegTemp();
				operat("load", y.register, b);
			}
			else b = y.register;
			operator("cmp_LT", a, b, a);
			reg = a;
		}
		else {
			String X;
			if (!x.name.equals("")) {
				X = newRegTemp();
				operat("load", x.register, X);
			}
			else X = x.register;
			String Y;
			if (!y.name.equals("")) {
				Y = newRegTemp();
				operat("load", y.register, Y);
			}
			else Y = y.register;
			println("call @functionLabel_stringLess " + register(X) + " " + register(Y));
			reg = newReg();
			operat("i2i", "global_return_register", reg);
		}
		if (ctx.op.getType() == MircnosParser.LE || ctx.op.getType() == MircnosParser.GE) {
			operatorI("xorI", reg, "1", reg);

		}
		return new Declaration("", "bool", 'V', reg);
	}

	@Override public Declaration visitUnary(MircnosParser.UnaryContext ctx) {
		Declaration x = visit(ctx.expression());
		String reg;
		if (x.name.length() > 0) {
			reg = newReg();
			operat("load", x.register, reg);
		}
		else reg = x.register;
		if (ctx.op.getType() == MircnosParser.SUB) {
			operatorI("rsubI", reg, "0", reg);
		}
		else {
			if (ctx.op.getType() == MircnosParser.INC)
				operatorI("addI", reg, "1", reg);
			else if (ctx.op.getType() == MircnosParser.DEC)
				operatorI("subI", reg, "1", reg);
			operat("store", reg, x.register);
		}
		return new Declaration("", "int", 'V', reg);
	}

	@Override public Declaration visitPrimary(MircnosParser.PrimaryContext ctx) {
//		System.out.println("Primary");
		if (ctx.expression() != null) {
//			System.out.println("expression");
			return visit(ctx.expression());
		}
		if (ctx.literal() != null) {
//			System.out.println("literal");
			return visit(ctx.literal());
		}
		if (ctx.Identifier() != null) {
			Declaration x = new Declaration(symbolTable.get(ctx.Identifier().getText()));
//			System.out.println(x.name);
			if (fnc.equals("print") || fnc.equals("println")) {
				String tmp = TMP;
				operat("load", x.register, tmp);
				printString(tmp);
			}
			return x;
		}
		return null;
	}

	@Override public Declaration visitNewType(MircnosParser.NewTypeContext ctx) {
		Declaration x = new Declaration();
		x.type = new String(visit(ctx.type()).type);
		for (int i = 0; i < ctx.expression().size(); ++i)
			x.type = x.type + "[]";
		for (int i = 0; i < ctx.square().size(); ++i)
			x.type = x.type + "[]";
		if (ctx.expression().size() > 0) {
			Declaration y = visit(ctx.expression(0));
			String sz;
			if (!y.name.equals("")) {
				sz = newRegTemp();
				operat("load", y.register, sz);
			}
			else sz = y.register;
			String sz4 = newRegTemp();
			String reg = newReg();
			operatorI("multI", sz, "8", sz4);
			operatorI("addI", sz4, "300", sz4);
			operat("malloc", sz4, reg);
			operatorI("addI", reg, "200", reg);
			operat("store", sz, reg);
			operatorI("addI", reg, "4", reg);
			x.register = reg;
//			operat("store", reg, x.register);
		}
		else {
			Declaration type= visit(ctx.type());
			String reg;
			if (type.type.equals("int") || type.type.equals("bool")) {
				reg = newInt();
			}
			else if (type.type.equals("string")) {
				reg = newString("");
			}
			else {
				reg = newClass(type.typeSize);
			}
			x.register = reg;
//			operat("store", reg, x.register);
		}
		return x;
	}

	public String newInt() {
		String reg = Integer.toString(++regtot);
		operatI("mallocI", "4", reg.toString());
		return reg;
	}

	public void newInt(String reg) {
		operatI("mallocI", "4", reg);
	}

	public String newString(String y) {
		String x = "";
		for (int i = 1; i < y.length() - 1; ++i) {
			if (y.charAt(i) == '\\' && i < y.length() - 2) {
				if (y.charAt(i + 1) == 'n') {
					x = x + "\n";
					++i;
					continue;
				}
				if (y.charAt(i + 1) == '\\') {
					x = x + "\\";
					++i;
					continue;
				}
				if (y.charAt(i + 1) == '"') {
					x = x + "\"";
					++i;
					continue;
				}
			}
			x = x + y.charAt(i);
		}
//		System.out.println(x);
		String reg = Integer.toString(++regtot);
		String tmp = TMP;
		int tot = 0;
		operatI("mallocI", Integer.toString((x.length() /4 + 2) * 4), reg);
		operatI("loadI", Integer.toString(x.length()), tmp);
		operat("store", tmp, reg);
		for (int i = 0; i <= x.length(); i += 4) {
			++tot;
			int k = 0;
			for (int j = 0; j < 4; ++j) {
				if (i + j < x.length())
					k += x.charAt(i + j) << (j << 3);
			}
			operatorI("addI", reg, Integer.toString(4), reg);
			operatI("loadI", Integer.toString(k), tmp);
			operat("store", tmp, reg);
		}
		operatorI("subI", reg, Integer.toString(tot * 4), reg);
		return reg;
	}

	public void newString(String reg, String y) {
		String x = "";
		for (int i = 1; i < y.length() - 1; ++i) {
			if (y.charAt(i) == '\\' && i < y.length() - 2) {
				if (y.charAt(i + 1) == 'n') {
					x = x + "\n";
					++i;
					continue;
				}
				if (y.charAt(i + 1) == '\\') {
					x = x + "\\";
					++i;
					continue;
				}
				if (y.charAt(i + 1) == '"') {
					x = x + "\"";
					++i;
					continue;
				}
			}
			x = x + y.charAt(i);
		}
		String tmp = newRegTemp();
		operatI("loadI", Integer.toString((x.length() /4 + 2) * 4), tmp);
		operat("malloc", tmp, reg);
		operatI("loadI", Integer.toString(x.length()), tmp);
		operat("store", tmp, reg);
		String tmp2 = newRegTemp();
		operat("i2i", reg, tmp);
		for (int i = 0; i <= x.length(); i += 4) {
			int k = 0;
			for (int j = 0; j < 4; ++j) {
				if (i + j < x.length())
					k += x.charAt(i + j) << (j << 3);
			}
			operatorI("addI", tmp, Integer.toString(4), tmp);
			operatI("loadI", Integer.toString(k), tmp2);
			operat("store", tmp2, tmp);
		}
	}

	public String newClass(Integer size) {
		Integer reg = ++regtot;
		operatI("mallocI", size.toString(), reg.toString());
		return reg.toString();
	}

	public void newClass(String reg, Integer size) {
		operatI("mallocI", size.toString(), reg);
	}

	public void putLabel(String l) {
		println("@Label_" + l + ":");
	}

	public String register(String r) {
		return "r_{" + r + "}";
	}

	public void operator (String op, String r1, String r2, String r3) {
		println(op + " " + register(r1) + " " + register(r2) + " " + register(r3));
	}

	public void operatorI (String op, String r1, String c2, String r3) {
		println(op + " " + register(r1) + " " + c2 + " " + register(r3));
	}

	public void operat (String op, String r1, String r2) {
		println(op + " " + register(r1) + " " + register(r2));
	}

	public void operatI (String op, String c1, String r2) {
		println(op + " " + c1 +  " " + register(r2));
	}

	public void jumpI(String l1) {
		println("jumpI " + "@Label_" + l1);
	}

	public void cbr(String r1, String l2, String l3) {
		println("cbr " + register(r1) + " " + "@Label_" + l2 + " " + "@Label_" + l3);
	}

	public void _return(String r1) {
		operat("i2i", r1, "global_return_register");
	}

	public void print(String out) {
		try {
			fileout.write(out);
		} catch (Exception e){
		}
	}

	public void println(String out) {
		try {
			fileout.write(out);
			fileout.newLine();
		} catch (Exception e) {
		}
	}

	String newReg() {
		return Integer.toString(++regtot);
	}

	String newRegTemp() {
		return newReg() + "_*temp";
	}

	String getReg(Declaration x) {
		String reg;
		if (x.name.length() > 0) {
			reg = newReg();
			operat("load", x.register, reg);
		}
		else reg = x.register;
		return reg;
	}

	String getRegTemp(Declaration x) {
		String reg;
		if (x.name.length() > 0) {
			reg = newRegTemp();
			operat("load", x.register, reg);
		}
		else reg = x.register;
		return reg;
	}

	void printString(String reg) {
		println("call @functionLabel_print " + register(reg));
	}
}

