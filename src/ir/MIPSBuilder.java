package ir;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.util.*;

class Operation {
	String op;
	ArrayList<String> args;

	Operation () {
		op = null;
		args = new ArrayList<String>();
	}
}

class Frame {
	public int fp;

	Frame() {
		fp = 0;
	}
}

public class MIPSBuilder {
	public BufferedReader filein;
	public BufferedWriter fileout;
	ArrayList<Operation> opr;
	HashMap<String, Integer> renum;
	String[] reg;
	Queue<String> que_;
	HashMap<String, String> rav;
	int rgs;
	int[] addr;
	HashMap<String, Frame> frm;
	Stack<String> stk;

	public void readIt() {
		rgs = 0;
		opr = new ArrayList<Operation>();
		renum = new HashMap<String, Integer>();
		String line = null;
		while ((line = getLine()) != null) {
//			System.out.println(line);
			Operation now = new Operation();
			int end = line.indexOf(' ');
			if (end == -1) end = line.length();
			now.op = line.substring(0, end);
			for (int i = now.op.length(), k = 0; i < line.length(); ) {
				int j = line.indexOf(' ', i + 1);
				if (j == -1) j = line.length();
				String arg = line.substring(i + 1, j);
				i = j;
				if (arg.length() == 0) continue;
				now.args.add(arg);
				if (arg.charAt(0) == 'r') {
					if (!renum.containsKey(arg)) renum.put(arg, rgs++);
				}
			}
			opr.add(now);
		}
	}

	public void calcMemory() {
		Frame fr = new Frame();
		String label = "";
		frm = new HashMap<String, Frame>();
		for (int i = 0; i < opr.size(); ++i) {
			if (opr.get(i).op.substring(0, 2).equals("@f")) {
				frm.put(label, fr);
				fr = new Frame();
				label = opr.get(i).op;
			}
			else {
				if (opr.get(i).args == null) continue;
				for (int j = 0; j < opr.get(i).args.size(); ++j) {
					String arg = opr.get(i).args.get(j);
					if (arg.charAt(0) == 'r') {
						if (addr[renum.get(arg)] == -1) {
							addr[renum.get(arg)] = fr.fp;
							fr.fp -= 4;
						}
					}
				}
			}
		}
		frm.put(label, fr);
	}

	public void translate() {
		println(".data");
		println("NEWLINE: .asciiz \"\\n\"");
		println("NEGATIVE: .asciiz \"-\"");
		println(".text");
		builtinFunction();
		reg = new String[renum.size()];
		for (int i = 0; i < renum.size(); ++i)
			reg[i] = null;
		addr = new int[renum.size()];
		for (int i = 0; i < renum.size(); ++i)
			addr[i] = -1;
		calcMemory();
//		System.out.println("sz == " + frm.size());
		String label = "";
//		System.out.println("size == " + opr.size());
		initialize();
		for (int i = 0; i < opr.size(); ++i) {
//			println((i + 1) + " " + opr.get(i).op);
//			System.out.println(opr.get(i).op);
//			clear();
			if (opr.get(i).op.charAt(0) == '@') {
				clear();
				if (opr.get(i).op.charAt(1) == 'f') {
					label = opr.get(i).op;
		//			initialize();
					if (label.equals("@functionLabel_main"))
						println("main:");
					else println(label.substring(1, label.length()) + ":");
					if (label.equals("@functionLabel_main"))
						println("move $fp $sp");
				}
				else {
					println(opr.get(i).op.substring(1, opr.get(i).op.length()));
				}
			}
			else if (opr.get(i).op.equals("return")) {
				clear();
				println("jr $ra");
			}
			else if (opr.get(i).op.equals("call")) {
				String func = opr.get(i).args.get(0);
				if (func.equals("@functionLabel__global")) {
					println("move $s7, $ra");
					println("jal " + func.substring(1, func.length()));
					println("move $ra, $s7");
					println("la $fp, " + frm.get(func).fp + "($sp)");
					continue;
				}
				if (func.equals("@functionLabel_print")) {
					String r = getReg(opr.get(i).args.get(1));
					println("li $v0, 4");
//					println("move $t9, $a0");
					println("la $a0, 4(" + r + ")");
					println("syscall");
//					println("move $a0, $t9");
					continue;
				}
				if (func.equals("@functionLabel_println")) {
					println("li $v0, 4");
					println("la $a0, NEWLINE");
					println("syscall");
					continue;
				}
				if (func.equals("@functionLabel_printInt")) {
					String r = getReg(opr.get(i).args.get(1));
					println("li $v0, 1");
					println("la $a0, (" + r + ")");
					println("syscall");
					continue;
				}
				if (func.equals("@functionLabel___builtin__length")) {
					println("lw $t9, ($s7)");
					continue;
				}
				if (func.equals("@functionLabel___builtin__ord")) {
					String r = getReg(opr.get(i).args.get(1));
					println("add " + r + ", " + r + ", $s7");
					println("lb $t9, (" + r + ")");
					continue;
				}
				if (func.equals("@functionLabel___builtin__size")) {
					println("lw $t9, -4($s7)");
					continue;
				}
				if (func.equals("@functionLabel_getInt")) {
					println("li $v0, 5");
					println("syscall");
					println("move $t9, $v0");
					continue;
				}
//				clear();
				for (int j = 1; j < opr.get(i).args.size() && j < 5; ++j) {
					String r = getReg(opr.get(i).args.get(j));
					println("move $a" + (j - 1) + ", " + r);
				}
				int fp = frm.get(label).fp - 4;
				println("sw $ra, " + frm.get(label).fp + "($fp)");
				if (opr.get(i).args.size() > 5)
					fp -= (opr.get(i).args.size() - 5) * 4;
				for (int j = 5; j < opr.get(i).args.size(); ++j) {
					String r = getReg(opr.get(i).args.get(j));
					println("sw " + r + ", " + (fp + (j - 4) * 4) + "($fp)");
				}
				clear();
				println("la $fp, " + fp + "($fp)");
				println("jal " + func.substring(1, func.length()));
				println("la $fp, " + (-fp) + "($fp)");
				println("lw $ra, " + frm.get(label).fp + "($fp)");
			}
			else if (opr.get(i).op.equals("mallocI")){
				int r = toInt(opr.get(i).args.get(0));
				println("move $t9, $a0");
				println("li $a0, " + r);
				println("li $v0, 9");
				println("syscall");
				String adr = getReg(opr.get(i).args.get(1));
				println("move " + adr + ", $v0");
				println("move $a0, $t9");
			}
			else if (opr.get(i).op.equals("malloc")){
				String r = getReg(opr.get(i).args.get(0));
				String adr = getReg(opr.get(i).args.get(1));
				println("li $v0, 9");
				println("move $t9, $a0");
				println("move $a0, " + r);
				println("syscall");
				println("move " + adr + ", $v0");
				println("move $a0, $t9");
			}
			else if (opr.get(i).op.equals("load")) {
				String r = getReg(opr.get(i).args.get(1));
				String adr = "(" + getReg(opr.get(i).args.get(0)) + ")";
				println("lw " + r + ", " + adr);
			}
			else if (opr.get(i).op.equals("loadI")) {
				String r = getReg(opr.get(i).args.get(1));
				println("li " + r + ", " + opr.get(i).args.get(0));
			}
			else if (opr.get(i).op.equals("store")) {
				String r = getReg(opr.get(i).args.get(0));
				String adr = "(" + getReg(opr.get(i).args.get(1)) + ")";
				println("sw " + r + ", " + adr);
				free(r);
			}
			else if (opr.get(i).op.equals("loadAI")) {
				String r = getReg(opr.get(i).args.get(2));
				String adr = toInt(opr.get(i).args.get(1)) + "(" + getReg(opr.get(i).args.get(0)) + ")";
				println("lw " + r + ", " + adr);
			}
			else if (opr.get(i).op.equals("i2i")) {
				String r0 = getReg(opr.get(i).args.get(0));
				String r1 = getReg(opr.get(i).args.get(1));
				println("move " + r1 + ", " + r0);
			}
			else if (opr.get(i).op.equals("jumpI")) {
				clear();
				println("b " + opr.get(i).args.get(0).substring(1, opr.get(i).args.get(0).length()));
			}
			else if (opr.get(i).op.equals("cbr")) {
				String r = getReg(opr.get(i).args.get(0));
				clear();
				println("beq " + r + ", 0, " + opr.get(i).args.get(2).substring(1, opr.get(i).args.get(2).length()));
				if (!opr.get(i).args.get(1).substring(7, 9).equals("if")) {
					println("b " + opr.get(i).args.get(1).substring(1, opr.get(i).args.get(1).length()));
				}
			}
			else if (opr.get(i).op.equals("rsubI")) {
				String r = getReg(opr.get(i).args.get(0));
				println("neg " + r + ", " + r);
			}
			else if (opr.get(i).op.equals("cmp_LT")) {
				String r0 = getReg(opr.get(i).args.get(0));
				String r1 = getReg(opr.get(i).args.get(1));
				String r2 = getReg(opr.get(i).args.get(2));
				println("slt " + r2 + ", " + r0 + ", " + r1);
			}
			else {
				String op = opr.get(i).op;
				if (op.charAt(op.length() - 1) == 'I')
					op = op.substring(0, op.length() - 1);
				if (op.equals("mult")) op = "mul";
				if (op.equals("lshift")) op = "sll";
				if (op.equals("rshift")) op = "srl";
//				System.out.println("??? " + op);
				String[] r = new String[3];
				for (int j = 0; j < 3; ++j)
					r[j] = getReg(opr.get(i).args.get(j));
//				System.out.println("???");
				println(op + " " + r[2] + ", " + r[0] + ", " + r[1]);
				if (!r[0].equals(r[2])) free(r[0]);
				if (!r[1].equals(r[2]) && r[1].charAt(0) == 'r') free(r[1]);
			}
		}
	}

	String getLine() {
		try{
			return filein.readLine();
		} catch (Exception e){
		}
		return null;
	}

	String getReg(String r) {
		if (r.equals("r_{global_return_register}")) return "$t9";
		if (r.equals("r_{global_builtin}")) return "$s7";
		if (r.charAt(0) != 'r') return r;
		String adr;
		int num = renum.get(r);
		if (r.contains("_arg_")) {
			int rn = toInt(r.substring(r.lastIndexOf('_') + 1, r.length() - 1));
//			System.out.println(r + " " + rn);
			if (rn < 4) return "$a" + rn;
			adr = (4 * (rn - 3)) + "($fp)";
		}
		else {
			adr = "$fp";
			if (r.length() > 10 && r.substring(3, 10).equals("global_")) adr = "$sp";
			adr = addr[num] + "(" + adr + ")";
		}
		if (reg[num] != null) {
			if (rav.containsKey(reg[num]) && rav.get(reg[num]).equals(r)) return reg[num];
		}
		String rg;
		boolean ntr = false;
		if (stk.size() > 0) {
			rg = stk.pop();
		}
		else {
			ntr = true;
			rg = que_.poll();
			free(rg);
		}
//		println("reg for " + r + " " + ntr);
		println("lw " + rg + ", " + adr);
		rav.put(rg, r);
		reg[num] = rg;
		que_.add(rg);
		if (ntr) stk.pop();
//		println("after " + stk.size() + " " + que_.size());
		return rg;
	}

	int toInt(String s) {
		boolean neg = s.charAt(0) == '-';
		if (neg) s = s.substring(1, s.length());
		int k = 0;
		for (int i = 0; i < s.length(); ++i)
			k = k * 10 + s.charAt(i) - '0';
		if (neg) k = -k;
		return k;
	}

	void free (String rg){
		if (!rav.containsKey(rg) || rg.charAt(1) == 'a' || rg.equals("$t9") || rg.charAt(0) != '$' || rg.equals("$s7")) return ;
		String base = "$fp";
		String r = rav.get(rg);
//		System.out.println(rg + " " + r);
		int num = renum.get(r);
		if (r.length() > 10 && r.substring(3, 10).equals("global_")) base = "$sp";
//		if (!r.contains("_*temp"))
			println("sw " + rg + ", " + addr[num] + "(" + base + ")");
		rav.remove(rg);
		stk.add(rg);
		que_.remove(rg);
	}

	int call(Frame fr) {
		int tot = -4;
		println("sw $ra, " + fr.fp + "($fp)");
		for (int i = 0; i < 8; ++i) {
			String r = "$s" + i;
			if (rav.containsKey(r)) {
				println("sw " + r + ", " + (fr.fp + (tot -= 4)) + "($fp)");
			}
		}
		for (int i = 0; i < 9; ++i) {
			String r = "$t" + i;
			if (rav.containsKey(r)) {
				println("sw " + r + ", " + (fr.fp + (tot -= 4)) + "($fp)");
			}
		}
		return fr.fp + tot;
	}

	void callEnd(Frame fr, int fp) {
		println("la $fp, " + fp + "($fp)");
		int tot = 0;
		println("lw $ra, " + fr.fp + "($fp)");
		for (int i = 0; i < 8; ++i) {
			String r = "$s" + i;
			if (rav.containsKey(r)) {
				println("lw " + r + ", " + (fr.fp + (tot -= 4)) + "($fp)");
			}
		}
		for (int i = 0; i < 9; ++i) {
			String r = "$t" + i;
			if (rav.containsKey(r)) {
				println("lw " + r + ", " + (fr.fp + (tot -= 4)) + "($fp)");
			}
		}
	}

	public void print(String s) {
		try {
			fileout.write(s);
		} catch (Exception e) {
		}
	}

	public void println(String s) {
		System.out.println(s);
		try {
			fileout.write(s);
			fileout.newLine();
		} catch (Exception e) {
		}
	}

	public void builtinFunction() {
		println("functionLabel_getString:");
		println("li $v0, 9");
//					println("move $t9, $a0");
		println("li $a0, 404");
		println("syscall");
		println("move $a0, $v0");
		println("la $a0, 4($a0)");
		println("li $a1, 396");
		println("li $v0, 8");
		println("syscall");
		println("li $t9, 0");
		println("move $a1, $a0");
		println("Label_getString_while:");
		println("lb $v0, ($a1)");
		println("beq $v0, 0, Label_getString_end");
		println("add $t9, $t9, 1");
		println("add $a1, $a1, 1");
		println("b Label_getString_while");
		println("Label_getString_end:");
		println("sw $t9, -4($a0)");
		println("add $t1, $t9, $a0");
		println("sb $zero, ($t1)");
		println("la $t9, -4($a0)");
		println("jr $ra");

		println("functionLabel___builtin__substring:");
		println("add $t9, $s7, 4");
		println("move $s1, $a0");
		println("move $s2, $a1");
		println("li $v0, 9");
		println("sub $a0, $s2, $s1");
		println("add $a0, $a0, 1");
		println("move $a1 $a0");
		println("div $a0, $a0, 4");
		println("mul $a0, $a0, 4");
		println("add $a0, $a0, 8");
		println("syscall");
		println("sw $a1, ($v0)");
		println("add $v0, $v0, 4");
		println("move $a2, $s1");
		println("li $t2, 0");
		println("Label_substring_while:");
		println("add $a0, $t9, $a2");
		println("lb $a0, ($a0)");
		println("add $a3, $v0, $t2");
		println("sb $a0, ($a3)");
		println("beq $a2, $s2, Label_substring_end");
		println("add $a2, $a2, 1");
		println("add $t2, $t2, 1");
		println("b Label_substring_while");
		println("Label_substring_end:");
		println("la $t9, -4($v0)");
		println("jr $ra");

		println("functionLabel_toString:");
		println("beq $a0, 0, Label_toString_zero");
		println("slt $t0, $a0, 0");
		println("beq $t0, 0, Label_toString_positive");
		println("sub $a0, $zero, $a0");
		println("Label_toString_positive:");
		println("move $s1, $a0");
		println("li $s0, 0");
		println("Label_toString_1_for:");
		println("beq $s1, 0, Label_toString_1_end");
		println("add $s0, $s0, 1");
		println("div $s1, $s1, 10");
		println("b Label_toString_1_for");
		println("Label_toString_1_end:");
		println("move $s6, $a0");
		println("add $s0, $s0, $t0");
		println("div $a0, $s0, 4");
		println("mul $a0, $a0, 4");
		println("add $a0, $a0, 8");
		println("li $v0, 9");
		println("syscall");
		println("sw $s0, ($v0)");
		println("add $v0, $v0, 4");
		println("add $s2, $v0, $s0");
		println("move $a0, $s6");
		println("Label_toString_2_for:");
		println("sub $s2, $s2, 1");
		println("div $t1, $a0, 10");
		println("mul $t1, $t1, 10");
		println("sub $t1, $a0, $t1");
		println("add $t1, $t1, 48");
		println("sb $t1, ($s2)");
		println("div $a0, $a0, 10");
		println("beq $a0, 0, Label_toString_2_end");
		println("b Label_toString_2_for");
		println("Label_toString_2_end:");
		println("la $t9, -4($v0)");
		println("beq $t0, 0, Label_toString_end");
		println("li $t0, 45");
		println("sb $t0, ($v0)");
		println("b Label_toString_end");
		println("Label_toString_zero:");
		println("li $v0, 9");
		println("li $a0, 8");
		println("syscall");
		println("li $t0, 1");
		println("sb $t0, ($v0)");
		println("li $t0, 48");
		println("sb $t0, 4($v0)");
		println("move $t9, $v0");
		println("Label_toString_end:");
		println("jr $ra");

		println("functionLabel___builtin__parseInt:");
		println("move $t9, $s7");
		println("add $s1, $t9, 4");
		println("lb $s2, ($s1)");
		println("li $s6, 0");
		println("bne $s2, 45, Label_parseInt_positive");
		println("add $s1, $s1, 1");
		println("li $s6, 1");
		println("Label_parseInt_positive:");
		println("li $t9, 0");
		println("Label_parseInt_for:");
		println("lb $s2, ($s1)");
		println("beq $s2, 0, Label_parseInt_end");
		println("sge $t1, $s2, 48");
		println("sle $t2, $s2, 57");
		println("and $t1, $t1, $t2");
		println("beq $t1, 0, Label_parseInt_end");
		println("mul $t9, $t9, 10");
		println("add $t9, $t9, $s2");
		println("sub $t9, $t9, 48");
		println("add $s1, $s1, 1");
		println("b Label_parseInt_for");
		println("Label_parseInt_end:");
		println("beq $s6, 0, Label_parseInt_over");
		println("neg $t9, $t9");
		println("Label_parseInt_over:");
		println("jr $ra");

		println("functionLabel_stringLess:");
		println("add $s0, $a0, 4");
		println("add $t0, $a1, 4");
		println("Label_stringLess_for:");
		println("lb $s1, ($s0)");
		println("lb $t1, ($t0)");
		println("bne $s1, $t1, Label_stringLess_end");
		println("beq $s1, 0, Label_stringLess_end");
		println("add $s0, $s0, 1");
		println("add $t0, $t0, 1");
		println("b Label_stringLess_for");
		println("Label_stringLess_end:");
		println("slt $t9, $s1, $t1");
		println("jr $ra");

		println("functionLabel_stringLink:");
		println("lw $s0, ($a0)");
		println("lw $t0, ($a1)");
		println("add $s6, $s0, $t0");
		println("add $s0, $a0, 4");
		println("add $t0, $a1, 4");
		println("li $v0, 9");
		println("div $a0, $s6, 4");
		println("mul $a0, $a0, 4");
		println("add $a0, $a0, 8");
		println("syscall");
		println("sw $s6, ($v0)");
		println("move $t9, $v0");
		println("add $s1, $v0, 4");
		println("Label_stringLink_1_for:");
		println("lb $t1, ($s0)");
		println("beq $t1, 0, Label_stringLink_1_end");
		println("sb $t1, ($s1)");
		println("add $s0, $s0, 1");
		println("add $s1, $s1, 1");
		println("b Label_stringLink_1_for");
		println("Label_stringLink_1_end:");
		println("Label_stringLink_2_for:");
		println("lb $t1, ($t0)");
		println("beq $t1, 0, Label_stringLink_2_end");
		println("sb $t1, ($s1)");
		println("add $t0, $t0, 1");
		println("add $s1, $s1, 1");
		println("b Label_stringLink_2_for");
		println("Label_stringLink_2_end:");
		println("li $t1, 0");
		println("sb $t1, ($s1)");
		println("jr $ra");
	}

	void clear () {
//		System.out.println("???");
		for (int j = 0; j < 9; ++j) {
			String r = "$t" + j;
//			System.out.println(r + " " + rav.get(r));
			if (rav.containsKey(r)) free(r);
		}
		for (int j = 0; j < 7; ++j) {
			String r = "$s" + j;
//			System.out.println(r + " " + rav.get(r));
			if (rav.containsKey(r)) free(r);
		}
//		System.out.println("f**k");
	}

	void initialize() {
		stk = new Stack<String>();
		que_= new LinkedList<String>();
		rav = new HashMap<String, String>();
		for (int i = 0; i < 9; ++i) stk.push("$t" + i);
		for (int i = 0; i < 7; ++i) stk.push("$s" + i);
	}

}
