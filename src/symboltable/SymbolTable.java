package symboltable;

import java.util.*;

public class SymbolTable {
	ArrayList<HashMap<String, Declaration> > map;
	int dep;

	Declaration print, println, getString, getInt, toString, length, substring, parseInt, ord, size;

	public boolean put(String name, String type, char dectype) {
		if (map.get(0).get(name) != null) {
			if (map.get(0).get(name).dectype != 'V') return false;
		}
		if (map.get(dep).get(name) != null) {
			return false;
		}
		map.get(dep).put(name, new Declaration(name, type, dectype));
		return true;
	}

	public boolean put(Declaration x) {
		if (map.get(0).get(x.name) != null) {
			if (map.get(0).get(x.name).dectype != 'V') return false;
		}
		if (map.get(dep).get(x.name) != null) {
			return false;
		}
		map.get(dep).put(x.name, x);
		return true;
	}

	public Declaration get(String x) {
		for (int i = dep; i >= 0; --i)
			if (map.get(i).get(x) != null) return map.get(i).get(x);
		return null;
	}

	public void down() {
		++dep;
		map.add(new HashMap<String, Declaration>());
	}

	public void up() {
		map.remove(dep);
		--dep;
	}

	public void stringDotSet() {
		down();
		map.get(dep).put(length.name, length);
		map.get(dep).put(substring.name, substring);
		map.get(dep).put(parseInt.name, parseInt);
		map.get(dep).put(ord.name, ord);
	}

	public void stringDotReset() {
		up();
	}

	public void arrayDotSet() {
		down();
		map.get(dep).put(size.name, size);
	}

	public void arrayDotReset() {
		up();
	}

	public int size() {
		int tot = 0;
		for (int i = 0; i <= dep; ++i)
			tot += map.get(i).size();
		return tot;
	}

	public SymbolTable() {
		map = new ArrayList<HashMap<String, Declaration> >();
		map.add(new HashMap<String, Declaration>());
		dep = 0;
		print = new Declaration("print", "void", 'F');
		print.put(new Variable("string", "str"));
		println = new Declaration("println", "void", 'F');
		println.put(new Variable("string", "str"));
		getString = new Declaration("getString", "string", 'F');
		getInt = new Declaration("getInt", "int", 'F');
		toString = new Declaration("toString", "string", 'F');
		toString.put(new Variable("int", "i"));
		length = new Declaration("length", "int", 'F');
		length.register = "__builtin__";
		substring = new Declaration("substring", "string", 'F');
		substring.put(new Variable("int", "left"));
		substring.put(new Variable("int", "right"));
		substring.register = "__builtin__";
		parseInt = new Declaration("parseInt", "int", 'F');
		parseInt.register = "__builtin__";
		ord = new Declaration("ord", "int", 'F');
		ord.put(new Variable("int", "pos"));
		ord.register = "__builtin__";
		size = new Declaration("size", "int", 'F');
		size.register = "__builtin__";
		put(print);
		put(println);
		put(getString);
		put(getInt);
		put(toString);

		put(new Declaration("int", "", 'C'));
		get("int").put("int", "this");
		put(new Declaration("string", "", 'C'));
		get("string").put("string", "this");
		put(new Declaration("bool", "", 'C'));
		get("bool").put("bool", "this");
		put(new Declaration("void", "", 'C'));
	}
}
