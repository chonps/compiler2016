package symboltable;

public class Variable {
	public String type, name;

	Variable(String _type, String _name) {
		type = new String(_type);
		name = new String(_name);
	}
}
