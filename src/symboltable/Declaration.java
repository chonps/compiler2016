package symboltable;

import java.util.ArrayList;
import java.util.HashMap;

public class Declaration {
	public String name;
	public char dectype;
	public String type;
	public ArrayList<Variable> funcList;
	public HashMap<String, String> typeList;
	public HashMap<String, Integer> sizeList;
	public String register;
	public Integer typeSize;

	public boolean put(Declaration x) {
		Variable u = new Variable(x.type, x.name);
		if (typeList.get(u.name) != null) return false;
		typeList.put(u.name, u.type);
		sizeList.put(u.name, typeSize);
		typeSize = typeSize.intValue() + 4;
		if (dectype == 'F') funcList.add(u);
		return true;
	}

	public boolean put(String type, String name) {
		Variable u = new Variable(type, name);
		if (typeList.get(u.name) != null) return false;
		typeList.put(u.name, u.type);
		sizeList.put(u.name, typeSize);
		typeSize = typeSize.intValue() + 4;
		if (dectype == 'F') funcList.add(u);
		return true;
	}

	public void put(Variable x) {
		typeList.put(x.name, x.type);
		funcList.add(x);
	}

	public Declaration() {
		name = "";
		type = "";
		funcList = new ArrayList<Variable>();
		typeList = new HashMap<String, String>();
		sizeList = new HashMap<String, Integer>();
		register = "";
		typeSize = 0;
	}

	public Declaration(String _name, String _type, char _dectype) {
		name = new String(_name);
		type = new String(_type);
		dectype = _dectype;
		funcList = new ArrayList<Variable>();
		typeList = new HashMap<String, String>();
		sizeList = new HashMap<String, Integer>();
		register = "";
		typeSize = 0;
	}

	public Declaration(String _name, String _type, char _dectype, String _register) {
		name = new String(_name);
		type = new String(_type);
		dectype = _dectype;
		funcList = new ArrayList<Variable>();
		typeList = new HashMap<String, String>();
		sizeList = new HashMap<String, Integer>();
		register = new String(_register);
		typeSize = 0;
	}

	public Declaration(Declaration x){
		name = new String(x.name);
		type = new String(x.type);
		dectype = x.dectype;
		funcList = new ArrayList<Variable>(x.funcList);
		typeList = new HashMap<String, String>(x.typeList);
		sizeList = new HashMap<String, Integer>(x.sizeList);
		register = new String(x.register);
		typeSize = new Integer(x.typeSize);
	}
}
