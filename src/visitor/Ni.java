package visitor;

import parser.*;
import symboltable.*;

public class Ni extends MircnosBaseVisitor<Declaration> {
	public SymbolTable symbolTable;
	String father;
	
	@Override public Declaration visitCompilationUnit(MircnosParser.CompilationUnitContext ctx) {
		for (int i = 0; i < ctx.declaration().size(); ++i)
			visit(ctx.declaration(i));
	 	return null;
	}

	@Override public Declaration visitDeclaration(MircnosParser.DeclarationContext ctx) {
		if (ctx.typeDeclaration() != null)
			visit(ctx.typeDeclaration());
		if (ctx.functionDeclaration() != null)
			visit(ctx.functionDeclaration());
		return null;
	}

	@Override public Declaration visitTypeDeclaration(MircnosParser.TypeDeclarationContext ctx) {
		if (ctx.classDeclaration() != null){
			visit(ctx.classDeclaration());
		}
		return null;
	}

	@Override public Declaration visitClassDeclaration(MircnosParser.ClassDeclarationContext ctx) {
		father = ctx.Identifier().getText();
		visit(ctx.classBody());
		return null;
	}

	@Override public Declaration visitClassBody(MircnosParser.ClassBodyContext ctx) {
		for (int i = 0; i < ctx.classBodyDeclaration().size(); ++i) {
			visit(ctx.classBodyDeclaration(i));
		}
		return null;
	}

	@Override public Declaration visitClassBodyDeclaration(MircnosParser.ClassBodyDeclarationContext ctx) {
		if (ctx.variableDeclarationStatement() != null) {
			visit(ctx.variableDeclarationStatement());
			return null;
		}
		return null;
	}

	@Override public Declaration visitFunctionDeclaration(MircnosParser.FunctionDeclarationContext ctx) {
		Declaration x = new Declaration();
		x.name = father = ctx.Identifier().getText();
		x.dectype = 'F';
		x.type = visit(ctx.type()).type;
		if (symbolTable.get(x.type) == null) throw new RuntimeException(ctx.getStart().getLine() + " " + x.type + " " + x.name + " Type Miss 2nd");
		if (symbolTable.get(x.type).dectype != 'C') throw new RuntimeException(ctx.getStart().getLine() + " " + x.type + " " + x.name + " Type Error 2nd");
		if (symbolTable.get(x.name) != null) throw new RuntimeException(ctx.getStart().getLine() + "Name Conflict");
		symbolTable.put(x);
		if (x.name.equals("main") && !x.type.equals("int")) throw new RuntimeException("Not int main");
		if (ctx.functionDeclarationParametersList() != null) {
			visit(ctx.functionDeclarationParametersList());
		}
		return x;
	}

	@Override public Declaration visitFunctionDeclarationParametersList(MircnosParser.FunctionDeclarationParametersListContext ctx) {
		for (int i = 0; i < ctx.variableDeclaration().size(); ++i) {
			visit(ctx.variableDeclaration(i));
		}
		return null;
	}

	@Override public Declaration visitType(MircnosParser.TypeContext ctx) {
		Declaration x = new Declaration();
		if (ctx.Identifier() != null) x.type = ctx.Identifier().getText();
		if (ctx.primitiveType() != null) x.type = visit(ctx.primitiveType()).type;
		if (ctx.type() != null) x.type = (new String(visit(ctx.type()).type)) + "[]";
		return x;
	}


	@Override public Declaration visitPrimitiveType(MircnosParser.PrimitiveTypeContext ctx) {
		Declaration x = new Declaration();
		x.type = ctx.getText();
		return x;
	}

	@Override public Declaration visitVariableDeclaration(MircnosParser.VariableDeclarationContext ctx) {
		Declaration x = new Declaration();
		x.type = visit(ctx.type()).type;
		x.name = ctx.Identifier().getText();
		x.dectype = 'V';
		if (x.name.equals(father)) throw new RuntimeException(ctx.getStart().getLine() + " Name Conflict");
		if (x.type.equals("void")) throw new RuntimeException(ctx.getStart().getLine() + " Type Illegal");
		int len = 0;
		for (; len < x.type.length(); ++len)
			if (x.type.charAt(len) == '[') break;
		if (symbolTable.get(x.type.substring(0, len)) == null) throw new RuntimeException(ctx.getStart().getLine() + " " + x.name + " Type Miss 2nd");
		if (!symbolTable.get(father).put(x)) throw new RuntimeException(ctx.getStart().getLine() + " " + x.name + "Variable Conflict");
		return x;
	}
}
