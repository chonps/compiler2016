package visitor;

import parser.*;
import symboltable.*;

public class Ichi extends MircnosBaseVisitor<Declaration> {
	public SymbolTable symbolTable;
	@Override public Declaration visitCompilationUnit(MircnosParser.CompilationUnitContext ctx) {
		for (int i = 0; i < ctx.declaration().size(); ++i)
			visit(ctx.declaration(i));
		return null;
	}

	@Override public Declaration visitDeclaration(MircnosParser.DeclarationContext ctx) {
		if (ctx.typeDeclaration() != null)
			visit(ctx.typeDeclaration());
		return null;
	}

	@Override public Declaration visitTypeDeclaration(MircnosParser.TypeDeclarationContext ctx) {
		if (ctx.classDeclaration() != null) visit(ctx.classDeclaration());
		return null;
	}

	@Override public Declaration visitClassDeclaration(MircnosParser.ClassDeclarationContext ctx) {
		Declaration x = new Declaration();
		x.name = x.type = ctx.Identifier().getText();
		x.dectype = 'C';
		if (symbolTable.get(x.name) != null) throw new RuntimeException(ctx.getStart().getLine() + " Name Confict");
		symbolTable.put(x);
		return null;
	}
}
