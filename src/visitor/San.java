package visitor;

import parser.*;
import symboltable.*;

public class San extends MircnosBaseVisitor<Declaration> {
	public SymbolTable symbolTable;
	boolean func;
	Declaration now;
	String blk;

	@Override public Declaration visitCompilationUnit(MircnosParser.CompilationUnitContext ctx) {
		func = false;
		blk = "global";
		for (int i = 0; i < ctx.declaration().size(); ++i)
			visit(ctx.declaration(i));
//		System.out.println("All End");
		return null;
	}

	@Override public Declaration visitDeclaration(MircnosParser.DeclarationContext ctx) {
		if (ctx.variableDeclarationStatement() != null)
			visit(ctx.variableDeclarationStatement());
		if (ctx.functionDeclaration() != null)
			visit(ctx.functionDeclaration());
		return null;
	}

	@Override public Declaration visitFunctionDeclaration(MircnosParser.FunctionDeclarationContext ctx) {
		Declaration x = symbolTable.get(ctx.Identifier().getText());
		symbolTable.down();
		for (int i = 0; i < x.funcList.size(); ++i){
			Declaration y = new Declaration(x.funcList.get(i).name, x.funcList.get(i).type, 'V');
			symbolTable.put(y);
		}
		now = x;
		blk = "function";
		visit(ctx.functionBody());
		blk = "global";
		symbolTable.up();
		return null;
	}


	@Override public Declaration visitFunctionBody(MircnosParser.FunctionBodyContext ctx) {
		visit(ctx.block());
		return null;
	}

	@Override public Declaration visitFunction(MircnosParser.FunctionContext ctx) {
		Declaration x = symbolTable.get(ctx.Identifier().getText());
		if (x == null) throw new RuntimeException(ctx.getStart().getLine() + " " + ctx.Identifier().getText() + " Not Define");
		if (x.dectype != 'F') throw new RuntimeException(ctx.getStart().getLine() + " " + x.name + "No Function");
		if (ctx.functionParametersList() == null) {
			if (x.funcList.size() != 0) throw new RuntimeException(ctx.getStart().getLine() + "Paraments Error");
		}
		else {
			Declaration lst = now;
			now = x;
			visit(ctx.functionParametersList());
			now = lst;
		}
		return x;
	}

	@Override public Declaration visitFunctionParametersList(MircnosParser.FunctionParametersListContext ctx) {
		if (ctx.functionParameters().size() != now.funcList.size()) throw new RuntimeException(ctx.getStart().getLine() + "Paraments Size Error");
		for (int i = 0; i < ctx.functionParameters().size(); ++i) {
			Declaration x = new Declaration();
			x.type = now.funcList.get(i).type;
			if (visit(ctx.functionParameters(i)).type.equals(now.funcList.get(i).type) == false && !(!x.type.equals("int") && !x.type.equals("bool") && !x.type.equals("string") && visit(ctx.functionParameters(i)).type.equals("null")))
				throw new RuntimeException(ctx.getStart().getLine() + " " + now.name + " " + i + " " + visit(ctx.functionParameters(i)).name + " " + visit(ctx.functionParameters(i)).type + " " + now.funcList.get(i).type + " Paraments Error");
		}
		return null;
	}


	@Override public Declaration visitFunctionParameters(MircnosParser.FunctionParametersContext ctx) {
		Declaration x = new Declaration();
//		System.out.println("ParaList");
		if (ctx.Identifier() != null) {
			if (symbolTable.get(ctx.Identifier().getText()) == null) throw new RuntimeException(ctx.getStart().getLine() + " " + ctx.Identifier().getText() + " Not Define");
			else x = symbolTable.get(ctx.Identifier().getText());
		}
		if (ctx.expression() != null){
//			System.out.println("It's expression");
			x = visit(ctx.expression());
		}
		return x;
	}

	@Override public Declaration visitType(MircnosParser.TypeContext ctx) {
		Declaration x = new Declaration();
		if (ctx.Identifier() != null) x.type = ctx.Identifier().getText();
		if (ctx.primitiveType() != null) x.type = visit(ctx.primitiveType()).type;
		if (ctx.type() != null) x.type = (new String(visit(ctx.type()).type)) + "[]";
		return x;
	}

	@Override public Declaration visitPrimitiveType(MircnosParser.PrimitiveTypeContext ctx) {
		Declaration x = new Declaration();
		x.type = ctx.getText();
		return x;
	}

	@Override public Declaration visitLiteral(MircnosParser.LiteralContext ctx) {
		Declaration x = new Declaration();
		if (ctx.IntegerLiteral() != null) x.type = "int";
		if (ctx.StringLiteral() != null) x.type = "string";
		if (ctx.BooleanLiteral() != null) x.type = "bool";
		if (ctx.NULL() != null) x.type = "null";
		return x;
	}

	@Override public Declaration visitBlock(MircnosParser.BlockContext ctx) {
		for (int i = 0; i < ctx.blockStatement().size(); ++i) {
//			System.out.println("blcokStatment " + i);
			visit(ctx.blockStatement(i));
		}
		return null;
	}

	@Override public Declaration visitBlockStatement(MircnosParser.BlockStatementContext ctx) {
		if (ctx.variableDeclarationStatement() != null) visit(ctx.variableDeclarationStatement());
		if (ctx.statement() != null) visit(ctx.statement());
		return null;
	}

	@Override public Declaration visitVariableDeclarationStatement(MircnosParser.VariableDeclarationStatementContext ctx) {
		visit(ctx.variableDeclaration());
		return null;
	}

	@Override public Declaration visitVariableDeclaration(MircnosParser.VariableDeclarationContext ctx) {
		Declaration x = new Declaration();
		x.type = visit(ctx.type()).type;
		if (x.type.equals("void")) throw new RuntimeException(ctx.getStart().getLine() + " Type Illegal");
		x.name = ctx.Identifier().getText();
		x.dectype = 'V';
		int len;
		for (len = 0; len < x.type.length(); ++len)
			if (x.type.charAt(len) == '[') break;
		if (symbolTable.get(x.type.substring(0, len)) == null) throw new RuntimeException(ctx.getStart().getLine() + " " + x.type.substring(0, len) + " " + x.name + " Type Miss 3rd");
		if (ctx.expression() != null) {
			if (x.type.equals(visit(ctx.expression()).type) == false && !(!x.type.equals("int") && !x.type.equals("bool") && !x.type.equals("string") && visit(ctx.expression()).type.equals("null"))) throw new RuntimeException(ctx.getStart().getLine() + " " + x.type + " " + visit(ctx.expression()).type + " " + x.name + " Type Conflict");
		}
		if (!symbolTable.put(x)) throw new RuntimeException(ctx.getStart().getLine() + "Variable Conflict");
		return x;
	}

	@Override public Declaration visitBlockStmt(MircnosParser.BlockStmtContext ctx) {
		symbolTable.down();
		visit(ctx.block());
		symbolTable.up();
		return null;
	}

	@Override public Declaration visitIf(MircnosParser.IfContext ctx) {
//		System.out.println("If Start");
		if (visit(ctx.parExpression()).type.equals("bool") == false) throw new RuntimeException(ctx.getStart().getLine() + " If Condition Error");
		visit(ctx.statement(0));
		if (ctx.statement().size() > 1) visit(ctx.statement(1));
//		System.out.println("If End");
		return null;
	}

	@Override public Declaration visitFor(MircnosParser.ForContext ctx) {
		String lst = new String(blk);
		blk = "for";
		symbolTable.down();
		visit(ctx.forControl());
		visit(ctx.statement());
		symbolTable.up();
		blk = lst;
		return null;
	}

	@Override public Declaration visitWhile(MircnosParser.WhileContext ctx) {
//		System.out.println("f**k");
		String lst = new String(blk);
		blk = "while";
		if (visit(ctx.parExpression()).type.equals("bool") == false) throw new RuntimeException(ctx.getStart().getLine() + " While Condition Error");
		visit(ctx.statement());
		blk = lst;
		return null;
	}

	@Override public Declaration visitReturn(MircnosParser.ReturnContext ctx) {
//		System.out.println("Return Prepare");
//		System.out.println(visit(ctx.expression()).type + " " + now.name + " " + now.type);
//		System.out.println("return " + now.name + " " + now.type);
		if (ctx.expression() != null) {
			if (visit(ctx.expression()).type.equals(now.type) == false)
				throw new RuntimeException(ctx.getStart().getLine() + " Return Type Eroor");
		}
		else{
			if (!now.type.equals("void"))
				throw new RuntimeException(ctx.getStart().getLine() + " Return Type Eroor");
		}
//		System.out.println("Excuse Me??????????");
		return null;
	}

	@Override public Declaration visitBreak(MircnosParser.BreakContext ctx) {
		if (!blk.equals("for") && !blk.equals("while")) throw new RuntimeException(ctx.getStart().getLine() + " Break Error");
		return null;
	}

	@Override public Declaration visitContinue(MircnosParser.ContinueContext ctx) {
		if (!blk.equals("for") && !blk.equals("while")) throw new RuntimeException(ctx.getStart().getLine() + " Continue Error");
		return null;
	}

	@Override public Declaration visitExpStmt(MircnosParser.ExpStmtContext ctx) {
		visit(ctx.statementExpression());
		return null;
	}

	@Override public Declaration visitForControl(MircnosParser.ForControlContext ctx) {
		if (ctx.forInit() != null) visit(ctx.forInit());
		if (ctx.expression() != null) {
			if (visit(ctx.expression()).type.equals("bool") == false) throw new RuntimeException(ctx.getStart().getLine() + "For Condition Error");
		}
		if (ctx.forUpdate() != null) visit(ctx.forUpdate());
		return null;
	}

	@Override public Declaration visitForInit(MircnosParser.ForInitContext ctx) {
		if (ctx.variableDeclaration() != null) visit(ctx.variableDeclaration());
		if (ctx.expression() != null) visit(ctx.expression());
		return null;
	}

	@Override public Declaration visitForUpdate(MircnosParser.ForUpdateContext ctx) {
		if (ctx.expression() != null) visit(ctx.expression());
		return null;
	}

	@Override public Declaration visitParExpression(MircnosParser.ParExpressionContext ctx) {
		return visit(ctx.expression());
	}

	@Override public Declaration visitStatementExpression(MircnosParser.StatementExpressionContext ctx) {
		visit(ctx.expression());
		return null;
	}

	@Override public Declaration visitPrim(MircnosParser.PrimContext ctx) {
		return visit(ctx.primary());
	}

	@Override public Declaration visitNew(MircnosParser.NewContext ctx) {
		return visit(ctx.newType());
	}

	@Override public Declaration visitLogicOr(MircnosParser.LogicOrContext ctx) {
		Declaration x = visit(ctx.expression(0)), y = visit(ctx.expression(1));
		if (!x.type.equals("bool") || !y.type.equals("bool")) throw new RuntimeException(ctx.getStart().getLine() + " Type Conflict");
		return x;
	}

	@Override public Declaration visitDotFunction(MircnosParser.DotFunctionContext ctx) {
		Declaration x = visit(ctx.expression());
		if (x.type.equals("string")) {
			symbolTable.stringDotSet();
			Declaration y = visit(ctx.function());
//			System.out.println("fuck");
			symbolTable.stringDotReset();
//			System.out.println("explode??? " + y.type);
			return y;
		}
		if (x.type.charAt(x.type.length() - 1) == ']') {
//			System.out.println("mdzz");
			symbolTable.arrayDotSet();
			Declaration y = visit(ctx.function());
			symbolTable.arrayDotReset();
			return y;
		}
		throw new RuntimeException(ctx.getStart().getLine() + "No Function");
	}

	@Override public Declaration visitOr(MircnosParser.OrContext ctx) {
		Declaration x = visit(ctx.expression(0)), y = visit(ctx.expression(1));
		if (!x.type.equals("int") || !y.type.equals("int")) throw new RuntimeException(ctx.getStart().getLine() + " Type Conflict");
		return x;
	}

	@Override public Declaration visitFunc(MircnosParser.FuncContext ctx) {
		return visit(ctx.function());
	}

	@Override public Declaration visitDot(MircnosParser.DotContext ctx) {
		Declaration x = visit(ctx.expression());
		String y = ctx.Identifier().getText();
		if (symbolTable.get(x.type).typeList.get(y) == null) throw new RuntimeException(ctx.getStart().getLine() + " Member Not Define");
		Declaration z = new Declaration();
		z.name = y;
		z.type = symbolTable.get(x.type).typeList.get(y);
		return z;
	}

	@Override public Declaration visitDouble(MircnosParser.DoubleContext ctx) {
		Declaration x = visit(ctx.expression());
		if (!x.type.equals("int")) throw new RuntimeException(ctx.getStart().getLine() + " Can't ++");
		return x;
	}

	@Override public Declaration visitAssignment(MircnosParser.AssignmentContext ctx) {
		Declaration x = visit(ctx.expression(0)), y = visit(ctx.expression(1));
		if (x.name.equals("") && (x.type.equals("int") || x.type.equals("string") || x.type.equals("bool") || x.type.equals("null"))) throw new RuntimeException(ctx.getStart().getLine() + " Not Variable");
		if (x.type.equals("null")) throw new RuntimeException(ctx.getStart().getLine() + "Type Illegal");
		if (!x.type.equals(y.type) && !(!x.type.equals("int") && !x.type.equals("bool") && !x.type.equals("string") && y.type.equals("null"))) throw new RuntimeException(ctx.getStart().getLine() + " Type Conflict");
		return y;
	}

	@Override public Declaration visitArray(MircnosParser.ArrayContext ctx) {
		Declaration x = new Declaration(visit(ctx.expression(0))), y = visit(ctx.expression(1));
		if (x.type.charAt(x.type.length() - 1) != ']') throw new RuntimeException(ctx.getStart().getLine() + " Not Array");
		x.type = x.type.substring(0, x.type.length() - 2);
		if (!y.type.equals("int")) throw new RuntimeException(ctx.getStart().getLine() + " Not Subscript");
		return x;
	}

	@Override public Declaration visitBitMove(MircnosParser.BitMoveContext ctx) {
		Declaration x = visit(ctx.expression(0)), y = visit(ctx.expression(1));
		if (!x.type.equals("int") || !y.type.equals("int")) throw new RuntimeException(ctx.getStart().getLine() + " Type Conflict");
		return x;
	}

	@Override public Declaration visitMult(MircnosParser.MultContext ctx) {
		Declaration x = visit(ctx.expression(0)), y = visit(ctx.expression(1));
		if (!x.type.equals("int") || !y.type.equals("int")) throw new RuntimeException(ctx.getStart().getLine() + " Type Conflict");
		return x;
	}

	@Override public Declaration visitEqual(MircnosParser.EqualContext ctx) {
		Declaration x = visit(ctx.expression(0)), y = visit(ctx.expression(1));
		if (x.type.equals("null")) {
			Declaration z;
			z = x;
			x = y;
			y = z;
		}
		if (!(x.type.equals(y.type) && (x.type.equals("int") || x.type.equals("string") || x.type.equals("bool"))) && !(!x.type.equals("int") && !x.type.equals("string") && !x.type.equals("bool") && y.type.equals("null"))) throw new RuntimeException(ctx.getStart().getLine() + " Type Conflict");
		return new Declaration("", "bool", 'V');
	}

	@Override public Declaration visitAnd(MircnosParser.AndContext ctx) {
		Declaration x = visit(ctx.expression(0)), y = visit(ctx.expression(1));
		if (!x.type.equals("int") || !y.type.equals("int")) throw new RuntimeException(ctx.getStart().getLine() + " Type Conflict");
		return x;
	}

	@Override public Declaration visitXor(MircnosParser.XorContext ctx) {
		Declaration x = visit(ctx.expression(0)), y = visit(ctx.expression(1));
		if (!x.type.equals("int") || !y.type.equals("int")) throw new RuntimeException(ctx.getStart().getLine() + " Type Conflict");
		return x;
	}

	@Override public Declaration visitDotThis(MircnosParser.DotThisContext ctx) {
		return visit(ctx.expression());
	}

	@Override public Declaration visitPlus(MircnosParser.PlusContext ctx) {
		Declaration x = visit(ctx.expression(0)), y = visit(ctx.expression(1));
		if (ctx.op.getType() == MircnosParser.ADD) {
			if (!x.type.equals(y.type) || (!x.type.equals("int") && !x.type.equals("string")))
				throw new RuntimeException(ctx.getStart().getLine() + " Type Conflict");
		}
		if (ctx.op.getType() == MircnosParser.SUB) {
			if (!x.type.equals(y.type) || !x.type.equals("int"))
				throw new RuntimeException(ctx.getStart().getLine() + " Type Conflict");
		}
		return x;
	}

	@Override public Declaration visitLogicAnd(MircnosParser.LogicAndContext ctx) {
		Declaration x = visit(ctx.expression(0)), y = visit(ctx.expression(1));
		if (!x.type.equals("bool") || !y.type.equals("bool")) throw new RuntimeException(ctx.getStart().getLine() + " Type Conflict");
		return x;
	}

	@Override public Declaration visitAnti(MircnosParser.AntiContext ctx) {
		Declaration x = visit(ctx.expression());
		if (!x.type.equals("int")) throw new RuntimeException(ctx.getStart().getLine() + " Type Conflict");
		return x;
	}

	@Override public Declaration visitNot(MircnosParser.NotContext ctx) {
		Declaration x = visit(ctx.expression());
		if (!x.type.equals("bool")) throw new RuntimeException(ctx.getStart().getLine() + " Type Conflict");
		return x;
	}

	@Override public Declaration visitLess(MircnosParser.LessContext ctx) {
		Declaration x = visit(ctx.expression(0)), y = visit(ctx.expression(1));
		if (!x.type.equals(y.type) || (!x.type.equals("int") && !x.type.equals("string"))) throw new RuntimeException(ctx.getStart().getLine() + " Type Conflict");
		return new Declaration("", "bool", 'V');
	}

	@Override public Declaration visitUnary(MircnosParser.UnaryContext ctx) {
		Declaration x = visit(ctx.expression());
		if (!x.type.equals("int")) throw new RuntimeException(ctx.getStart().getLine() + " Type Conflict");
		return x;
	}

	@Override public Declaration visitPrimary(MircnosParser.PrimaryContext ctx) {
		if (ctx.expression() != null) {
			return visit(ctx.expression());
		}
		if (ctx.literal() != null) {
			return visit(ctx.literal());
		}
		if (ctx.Identifier() != null) {
			Declaration x = symbolTable.get(ctx.Identifier().getText());
			if (x == null) throw new RuntimeException(ctx.getStart().getLine() + " " + ctx.Identifier().getText() + " Not Define");
			else {
				if (x.dectype != 'V') throw new RuntimeException(ctx.getStart().getLine() + " " + x.name + "Not Define");
				return x;
			}
		}
		return null;
	}

	@Override public Declaration visitNewType(MircnosParser.NewTypeContext ctx) {
		Declaration x = new Declaration();
		x.type = new String(visit(ctx.type()).type);
		for (int i = 0; i < ctx.expression().size(); ++i)
			x.type = x.type + "[]";
		for (int i = 0; i < ctx.square().size(); ++i)
			x.type = x.type + "[]";
		return x;
	}
}
