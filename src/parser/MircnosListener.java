package parser;

// Generated from Mircnos.g4 by ANTLR 4.5.3
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link MircnosParser}.
 */
public interface MircnosListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link MircnosParser#compilationUnit}.
	 * @param ctx the parse tree
	 */
	void enterCompilationUnit(MircnosParser.CompilationUnitContext ctx);
	/**
	 * Exit a parse tree produced by {@link MircnosParser#compilationUnit}.
	 * @param ctx the parse tree
	 */
	void exitCompilationUnit(MircnosParser.CompilationUnitContext ctx);
	/**
	 * Enter a parse tree produced by {@link MircnosParser#declaration}.
	 * @param ctx the parse tree
	 */
	void enterDeclaration(MircnosParser.DeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link MircnosParser#declaration}.
	 * @param ctx the parse tree
	 */
	void exitDeclaration(MircnosParser.DeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link MircnosParser#typeDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterTypeDeclaration(MircnosParser.TypeDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link MircnosParser#typeDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitTypeDeclaration(MircnosParser.TypeDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link MircnosParser#classDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterClassDeclaration(MircnosParser.ClassDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link MircnosParser#classDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitClassDeclaration(MircnosParser.ClassDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link MircnosParser#classBody}.
	 * @param ctx the parse tree
	 */
	void enterClassBody(MircnosParser.ClassBodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link MircnosParser#classBody}.
	 * @param ctx the parse tree
	 */
	void exitClassBody(MircnosParser.ClassBodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link MircnosParser#classBodyDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterClassBodyDeclaration(MircnosParser.ClassBodyDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link MircnosParser#classBodyDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitClassBodyDeclaration(MircnosParser.ClassBodyDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link MircnosParser#functionDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterFunctionDeclaration(MircnosParser.FunctionDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link MircnosParser#functionDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitFunctionDeclaration(MircnosParser.FunctionDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link MircnosParser#functionDeclarationParametersList}.
	 * @param ctx the parse tree
	 */
	void enterFunctionDeclarationParametersList(MircnosParser.FunctionDeclarationParametersListContext ctx);
	/**
	 * Exit a parse tree produced by {@link MircnosParser#functionDeclarationParametersList}.
	 * @param ctx the parse tree
	 */
	void exitFunctionDeclarationParametersList(MircnosParser.FunctionDeclarationParametersListContext ctx);
	/**
	 * Enter a parse tree produced by {@link MircnosParser#functionBody}.
	 * @param ctx the parse tree
	 */
	void enterFunctionBody(MircnosParser.FunctionBodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link MircnosParser#functionBody}.
	 * @param ctx the parse tree
	 */
	void exitFunctionBody(MircnosParser.FunctionBodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link MircnosParser#function}.
	 * @param ctx the parse tree
	 */
	void enterFunction(MircnosParser.FunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MircnosParser#function}.
	 * @param ctx the parse tree
	 */
	void exitFunction(MircnosParser.FunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MircnosParser#functionParametersList}.
	 * @param ctx the parse tree
	 */
	void enterFunctionParametersList(MircnosParser.FunctionParametersListContext ctx);
	/**
	 * Exit a parse tree produced by {@link MircnosParser#functionParametersList}.
	 * @param ctx the parse tree
	 */
	void exitFunctionParametersList(MircnosParser.FunctionParametersListContext ctx);
	/**
	 * Enter a parse tree produced by {@link MircnosParser#functionParameters}.
	 * @param ctx the parse tree
	 */
	void enterFunctionParameters(MircnosParser.FunctionParametersContext ctx);
	/**
	 * Exit a parse tree produced by {@link MircnosParser#functionParameters}.
	 * @param ctx the parse tree
	 */
	void exitFunctionParameters(MircnosParser.FunctionParametersContext ctx);
	/**
	 * Enter a parse tree produced by {@link MircnosParser#type}.
	 * @param ctx the parse tree
	 */
	void enterType(MircnosParser.TypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link MircnosParser#type}.
	 * @param ctx the parse tree
	 */
	void exitType(MircnosParser.TypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link MircnosParser#primitiveType}.
	 * @param ctx the parse tree
	 */
	void enterPrimitiveType(MircnosParser.PrimitiveTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link MircnosParser#primitiveType}.
	 * @param ctx the parse tree
	 */
	void exitPrimitiveType(MircnosParser.PrimitiveTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link MircnosParser#literal}.
	 * @param ctx the parse tree
	 */
	void enterLiteral(MircnosParser.LiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link MircnosParser#literal}.
	 * @param ctx the parse tree
	 */
	void exitLiteral(MircnosParser.LiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link MircnosParser#block}.
	 * @param ctx the parse tree
	 */
	void enterBlock(MircnosParser.BlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link MircnosParser#block}.
	 * @param ctx the parse tree
	 */
	void exitBlock(MircnosParser.BlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link MircnosParser#blockStatement}.
	 * @param ctx the parse tree
	 */
	void enterBlockStatement(MircnosParser.BlockStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MircnosParser#blockStatement}.
	 * @param ctx the parse tree
	 */
	void exitBlockStatement(MircnosParser.BlockStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link MircnosParser#variableDeclarationStatement}.
	 * @param ctx the parse tree
	 */
	void enterVariableDeclarationStatement(MircnosParser.VariableDeclarationStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MircnosParser#variableDeclarationStatement}.
	 * @param ctx the parse tree
	 */
	void exitVariableDeclarationStatement(MircnosParser.VariableDeclarationStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link MircnosParser#variableDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterVariableDeclaration(MircnosParser.VariableDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link MircnosParser#variableDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitVariableDeclaration(MircnosParser.VariableDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BlockStmt}
	 * labeled alternative in {@link MircnosParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterBlockStmt(MircnosParser.BlockStmtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BlockStmt}
	 * labeled alternative in {@link MircnosParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitBlockStmt(MircnosParser.BlockStmtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code If}
	 * labeled alternative in {@link MircnosParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterIf(MircnosParser.IfContext ctx);
	/**
	 * Exit a parse tree produced by the {@code If}
	 * labeled alternative in {@link MircnosParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitIf(MircnosParser.IfContext ctx);
	/**
	 * Enter a parse tree produced by the {@code For}
	 * labeled alternative in {@link MircnosParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterFor(MircnosParser.ForContext ctx);
	/**
	 * Exit a parse tree produced by the {@code For}
	 * labeled alternative in {@link MircnosParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitFor(MircnosParser.ForContext ctx);
	/**
	 * Enter a parse tree produced by the {@code While}
	 * labeled alternative in {@link MircnosParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterWhile(MircnosParser.WhileContext ctx);
	/**
	 * Exit a parse tree produced by the {@code While}
	 * labeled alternative in {@link MircnosParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitWhile(MircnosParser.WhileContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Return}
	 * labeled alternative in {@link MircnosParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterReturn(MircnosParser.ReturnContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Return}
	 * labeled alternative in {@link MircnosParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitReturn(MircnosParser.ReturnContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Break}
	 * labeled alternative in {@link MircnosParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterBreak(MircnosParser.BreakContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Break}
	 * labeled alternative in {@link MircnosParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitBreak(MircnosParser.BreakContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Continue}
	 * labeled alternative in {@link MircnosParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterContinue(MircnosParser.ContinueContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Continue}
	 * labeled alternative in {@link MircnosParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitContinue(MircnosParser.ContinueContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Empty}
	 * labeled alternative in {@link MircnosParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterEmpty(MircnosParser.EmptyContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Empty}
	 * labeled alternative in {@link MircnosParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitEmpty(MircnosParser.EmptyContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ExpStmt}
	 * labeled alternative in {@link MircnosParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterExpStmt(MircnosParser.ExpStmtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ExpStmt}
	 * labeled alternative in {@link MircnosParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitExpStmt(MircnosParser.ExpStmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link MircnosParser#forControl}.
	 * @param ctx the parse tree
	 */
	void enterForControl(MircnosParser.ForControlContext ctx);
	/**
	 * Exit a parse tree produced by {@link MircnosParser#forControl}.
	 * @param ctx the parse tree
	 */
	void exitForControl(MircnosParser.ForControlContext ctx);
	/**
	 * Enter a parse tree produced by {@link MircnosParser#forInit}.
	 * @param ctx the parse tree
	 */
	void enterForInit(MircnosParser.ForInitContext ctx);
	/**
	 * Exit a parse tree produced by {@link MircnosParser#forInit}.
	 * @param ctx the parse tree
	 */
	void exitForInit(MircnosParser.ForInitContext ctx);
	/**
	 * Enter a parse tree produced by {@link MircnosParser#forUpdate}.
	 * @param ctx the parse tree
	 */
	void enterForUpdate(MircnosParser.ForUpdateContext ctx);
	/**
	 * Exit a parse tree produced by {@link MircnosParser#forUpdate}.
	 * @param ctx the parse tree
	 */
	void exitForUpdate(MircnosParser.ForUpdateContext ctx);
	/**
	 * Enter a parse tree produced by {@link MircnosParser#parExpression}.
	 * @param ctx the parse tree
	 */
	void enterParExpression(MircnosParser.ParExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MircnosParser#parExpression}.
	 * @param ctx the parse tree
	 */
	void exitParExpression(MircnosParser.ParExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MircnosParser#statementExpression}.
	 * @param ctx the parse tree
	 */
	void enterStatementExpression(MircnosParser.StatementExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MircnosParser#statementExpression}.
	 * @param ctx the parse tree
	 */
	void exitStatementExpression(MircnosParser.StatementExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Prim}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterPrim(MircnosParser.PrimContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Prim}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitPrim(MircnosParser.PrimContext ctx);
	/**
	 * Enter a parse tree produced by the {@code New}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterNew(MircnosParser.NewContext ctx);
	/**
	 * Exit a parse tree produced by the {@code New}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitNew(MircnosParser.NewContext ctx);
	/**
	 * Enter a parse tree produced by the {@code LogicOr}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterLogicOr(MircnosParser.LogicOrContext ctx);
	/**
	 * Exit a parse tree produced by the {@code LogicOr}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitLogicOr(MircnosParser.LogicOrContext ctx);
	/**
	 * Enter a parse tree produced by the {@code DotFunction}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterDotFunction(MircnosParser.DotFunctionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code DotFunction}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitDotFunction(MircnosParser.DotFunctionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Or}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterOr(MircnosParser.OrContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Or}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitOr(MircnosParser.OrContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Func}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterFunc(MircnosParser.FuncContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Func}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitFunc(MircnosParser.FuncContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Dot}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterDot(MircnosParser.DotContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Dot}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitDot(MircnosParser.DotContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Double}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterDouble(MircnosParser.DoubleContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Double}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitDouble(MircnosParser.DoubleContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Unary}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterUnary(MircnosParser.UnaryContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Unary}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitUnary(MircnosParser.UnaryContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Assignment}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterAssignment(MircnosParser.AssignmentContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Assignment}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitAssignment(MircnosParser.AssignmentContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Array}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterArray(MircnosParser.ArrayContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Array}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitArray(MircnosParser.ArrayContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BitMove}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterBitMove(MircnosParser.BitMoveContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BitMove}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitBitMove(MircnosParser.BitMoveContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Not}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterNot(MircnosParser.NotContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Not}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitNot(MircnosParser.NotContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Mult}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterMult(MircnosParser.MultContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Mult}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitMult(MircnosParser.MultContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Equal}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterEqual(MircnosParser.EqualContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Equal}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitEqual(MircnosParser.EqualContext ctx);
	/**
	 * Enter a parse tree produced by the {@code And}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterAnd(MircnosParser.AndContext ctx);
	/**
	 * Exit a parse tree produced by the {@code And}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitAnd(MircnosParser.AndContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Xor}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterXor(MircnosParser.XorContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Xor}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitXor(MircnosParser.XorContext ctx);
	/**
	 * Enter a parse tree produced by the {@code DotThis}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterDotThis(MircnosParser.DotThisContext ctx);
	/**
	 * Exit a parse tree produced by the {@code DotThis}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitDotThis(MircnosParser.DotThisContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Plus}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterPlus(MircnosParser.PlusContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Plus}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitPlus(MircnosParser.PlusContext ctx);
	/**
	 * Enter a parse tree produced by the {@code LogicAnd}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterLogicAnd(MircnosParser.LogicAndContext ctx);
	/**
	 * Exit a parse tree produced by the {@code LogicAnd}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitLogicAnd(MircnosParser.LogicAndContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Anti}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterAnti(MircnosParser.AntiContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Anti}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitAnti(MircnosParser.AntiContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Less}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterLess(MircnosParser.LessContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Less}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitLess(MircnosParser.LessContext ctx);
	/**
	 * Enter a parse tree produced by {@link MircnosParser#primary}.
	 * @param ctx the parse tree
	 */
	void enterPrimary(MircnosParser.PrimaryContext ctx);
	/**
	 * Exit a parse tree produced by {@link MircnosParser#primary}.
	 * @param ctx the parse tree
	 */
	void exitPrimary(MircnosParser.PrimaryContext ctx);
	/**
	 * Enter a parse tree produced by {@link MircnosParser#newType}.
	 * @param ctx the parse tree
	 */
	void enterNewType(MircnosParser.NewTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link MircnosParser#newType}.
	 * @param ctx the parse tree
	 */
	void exitNewType(MircnosParser.NewTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link MircnosParser#square}.
	 * @param ctx the parse tree
	 */
	void enterSquare(MircnosParser.SquareContext ctx);
	/**
	 * Exit a parse tree produced by {@link MircnosParser#square}.
	 * @param ctx the parse tree
	 */
	void exitSquare(MircnosParser.SquareContext ctx);
}