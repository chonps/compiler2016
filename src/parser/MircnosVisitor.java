package parser;

// Generated from Mircnos.g4 by ANTLR 4.5.3
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link MircnosParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface MircnosVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link MircnosParser#compilationUnit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCompilationUnit(MircnosParser.CompilationUnitContext ctx);
	/**
	 * Visit a parse tree produced by {@link MircnosParser#declaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclaration(MircnosParser.DeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link MircnosParser#typeDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypeDeclaration(MircnosParser.TypeDeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link MircnosParser#classDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassDeclaration(MircnosParser.ClassDeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link MircnosParser#classBody}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassBody(MircnosParser.ClassBodyContext ctx);
	/**
	 * Visit a parse tree produced by {@link MircnosParser#classBodyDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassBodyDeclaration(MircnosParser.ClassBodyDeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link MircnosParser#functionDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionDeclaration(MircnosParser.FunctionDeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link MircnosParser#functionDeclarationParametersList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionDeclarationParametersList(MircnosParser.FunctionDeclarationParametersListContext ctx);
	/**
	 * Visit a parse tree produced by {@link MircnosParser#functionBody}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionBody(MircnosParser.FunctionBodyContext ctx);
	/**
	 * Visit a parse tree produced by {@link MircnosParser#function}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction(MircnosParser.FunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link MircnosParser#functionParametersList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionParametersList(MircnosParser.FunctionParametersListContext ctx);
	/**
	 * Visit a parse tree produced by {@link MircnosParser#functionParameters}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionParameters(MircnosParser.FunctionParametersContext ctx);
	/**
	 * Visit a parse tree produced by {@link MircnosParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType(MircnosParser.TypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link MircnosParser#primitiveType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrimitiveType(MircnosParser.PrimitiveTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link MircnosParser#literal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiteral(MircnosParser.LiteralContext ctx);
	/**
	 * Visit a parse tree produced by {@link MircnosParser#block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlock(MircnosParser.BlockContext ctx);
	/**
	 * Visit a parse tree produced by {@link MircnosParser#blockStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlockStatement(MircnosParser.BlockStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link MircnosParser#variableDeclarationStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariableDeclarationStatement(MircnosParser.VariableDeclarationStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link MircnosParser#variableDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariableDeclaration(MircnosParser.VariableDeclarationContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BlockStmt}
	 * labeled alternative in {@link MircnosParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlockStmt(MircnosParser.BlockStmtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code If}
	 * labeled alternative in {@link MircnosParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf(MircnosParser.IfContext ctx);
	/**
	 * Visit a parse tree produced by the {@code For}
	 * labeled alternative in {@link MircnosParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFor(MircnosParser.ForContext ctx);
	/**
	 * Visit a parse tree produced by the {@code While}
	 * labeled alternative in {@link MircnosParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhile(MircnosParser.WhileContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Return}
	 * labeled alternative in {@link MircnosParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReturn(MircnosParser.ReturnContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Break}
	 * labeled alternative in {@link MircnosParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBreak(MircnosParser.BreakContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Continue}
	 * labeled alternative in {@link MircnosParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitContinue(MircnosParser.ContinueContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Empty}
	 * labeled alternative in {@link MircnosParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEmpty(MircnosParser.EmptyContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ExpStmt}
	 * labeled alternative in {@link MircnosParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpStmt(MircnosParser.ExpStmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link MircnosParser#forControl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForControl(MircnosParser.ForControlContext ctx);
	/**
	 * Visit a parse tree produced by {@link MircnosParser#forInit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForInit(MircnosParser.ForInitContext ctx);
	/**
	 * Visit a parse tree produced by {@link MircnosParser#forUpdate}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForUpdate(MircnosParser.ForUpdateContext ctx);
	/**
	 * Visit a parse tree produced by {@link MircnosParser#parExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParExpression(MircnosParser.ParExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link MircnosParser#statementExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatementExpression(MircnosParser.StatementExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Prim}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrim(MircnosParser.PrimContext ctx);
	/**
	 * Visit a parse tree produced by the {@code New}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNew(MircnosParser.NewContext ctx);
	/**
	 * Visit a parse tree produced by the {@code LogicOr}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogicOr(MircnosParser.LogicOrContext ctx);
	/**
	 * Visit a parse tree produced by the {@code DotFunction}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDotFunction(MircnosParser.DotFunctionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Or}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOr(MircnosParser.OrContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Func}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunc(MircnosParser.FuncContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Dot}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDot(MircnosParser.DotContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Double}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDouble(MircnosParser.DoubleContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Unary}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnary(MircnosParser.UnaryContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Assignment}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignment(MircnosParser.AssignmentContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Array}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArray(MircnosParser.ArrayContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BitMove}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitMove(MircnosParser.BitMoveContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Not}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNot(MircnosParser.NotContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Mult}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMult(MircnosParser.MultContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Equal}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEqual(MircnosParser.EqualContext ctx);
	/**
	 * Visit a parse tree produced by the {@code And}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAnd(MircnosParser.AndContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Xor}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitXor(MircnosParser.XorContext ctx);
	/**
	 * Visit a parse tree produced by the {@code DotThis}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDotThis(MircnosParser.DotThisContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Plus}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPlus(MircnosParser.PlusContext ctx);
	/**
	 * Visit a parse tree produced by the {@code LogicAnd}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogicAnd(MircnosParser.LogicAndContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Anti}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAnti(MircnosParser.AntiContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Less}
	 * labeled alternative in {@link MircnosParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLess(MircnosParser.LessContext ctx);
	/**
	 * Visit a parse tree produced by {@link MircnosParser#primary}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrimary(MircnosParser.PrimaryContext ctx);
	/**
	 * Visit a parse tree produced by {@link MircnosParser#newType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNewType(MircnosParser.NewTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link MircnosParser#square}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSquare(MircnosParser.SquareContext ctx);
}