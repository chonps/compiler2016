grammar Mircnos;

// starting point for parsing a java file

compilationUnit
	:   declaration* EOF
        EOF
    ;

declaration
	:   typeDeclaration
    |   functionDeclaration
    |   variableDeclarationStatement
	;

typeDeclaration
    :   classDeclaration
    |   ';'
    ;

classDeclaration
    :   'class' Identifier classBody
    ;

classBody
    :   '{' classBodyDeclaration* '}'
    ;

classBodyDeclaration
    :   ';'
    |   variableDeclarationStatement
    ;

functionDeclaration
	:   type Identifier '(' functionDeclarationParametersList? ')' functionBody
	;

functionDeclarationParametersList
	:   variableDeclaration (',' variableDeclaration)*
	;

functionBody
	:   block
	;

function
	:   Identifier '(' functionParametersList? ')'
	;

functionParametersList
	:   functionParameters (',' functionParameters)*
	;

functionParameters
	:   (Identifier | expression)
	;

type
	:   Identifier
	|   primitiveType
	|   type'[]'
    ;

primitiveType
    :   'bool'
    |   'int'
    |   'string'
    |   'void'
    ;

literal
    :   IntegerLiteral
    |   StringLiteral
    |   BooleanLiteral
    |   NULL
    ;

// STATEMENTS / BLOCKS

block
    :   '{' blockStatement* '}'
    ;

blockStatement
    :   variableDeclarationStatement
    |   statement
    ;

variableDeclarationStatement
    :    variableDeclaration ';'
    ;

variableDeclaration
    :   type Identifier ('=' expression)?
    ;

statement
    :   block #BlockStmt
    |   'if' parExpression statement ('else' statement)? #If
    |   'for' '(' forControl ')' statement #For
    |   'while' parExpression statement #While
    |   'return' expression? ';' #Return
    |   'break' ';' #Break
    |   'continue' ';' #Continue
    |   ';' #Empty
    |   statementExpression ';' #ExpStmt
    ;

forControl
    :   forInit? ';' expression? ';' forUpdate?
    ;

forInit
    :   variableDeclaration
    |   expression
    ;

forUpdate
    :   expression
    ;

// EXPRESSIONS

parExpression
    :   '(' expression ')'
    ;

statementExpression
    :   expression
    ;

expression
	:   function #Func
    |   'new' newType #New
    |   expression '.' Identifier #Dot
    |   expression '.' 'this' #DotThis
    |   expression '.' function #DotFunction
    |   expression '[' expression ']' #Array
    |   expression op=('++' | '--') #Double
    |   op=('+'|'-'|'++'|'--') expression #Unary
    |   '~' expression #Anti
    |   '!' expression #Not
    |   expression op=('*'|'/'|'%') expression #Mult
    |   expression op=('+'|'-') expression #Plus
    |   expression op=('<<' | '>>') expression #BitMove
    |   expression op=('<=' | '>=' | '>' | '<') expression #Less
    |   expression op=('==' | '!=') expression #Equal
    |   expression '&' expression #And
    |   expression '^' expression #Xor
    |   expression '|' expression #Or
    |   expression '&&' expression #LogicAnd
    |   expression '||' expression #LogicOr
    |   <assoc=right> expression '=' expression #Assignment
    |   primary #Prim
    ;

primary
    :   '(' expression ')'
    |   literal
    |   Identifier
    ;

newType
	:   type ('[' expression ']')* (square)*
	;

square
	:   '[]'
	;

// LEXER

// §3.9 Keywords

BOOL          : 'bool';
BREAK         : 'break';
CLASS         : 'class';
CONTINUE      : 'continue';
ELSE          : 'else';
FOR           : 'for';
IF            : 'if';
INT           : 'int';
NEW           : 'new';
RETURN        : 'return';
VOID          : 'void';
NULL          : 'null';
STRING        : 'string';
WHILE         : 'while';

// §3.10.1 Integer Literals

IntegerLiteral
    :   DecimalIntegerLiteral
    ;

fragment
DecimalIntegerLiteral
    :   DecimalNumeral IntegerTypeSuffix?
    ;

fragment
IntegerTypeSuffix
    :   [lL]
    ;

fragment
DecimalNumeral
    :   '0'
    |   NonZeroDigit (Digits? | Underscores Digits)
    ;

fragment
Digits
    :   Digit (DigitOrUnderscore* Digit)?
    ;

fragment
Digit
    :   '0'
    |   NonZeroDigit
    ;

fragment
NonZeroDigit
    :   [1-9]
    ;

fragment
DigitOrUnderscore
    :   Digit
    |   '_'
    ;

fragment
Underscores
    :   '_'+
    ;

// §3.10.3 Boolean Literals

BooleanLiteral
    :   'true'
    |   'false'
    ;

TRUE    : 'true';
FALSE   : 'false';

// §3.10.5 String Literals

StringLiteral
    :   '"' StringCharacters? '"'
    ;

fragment
StringCharacters
    :   StringCharacter+
    ;

fragment
StringCharacter
    :   ~["\\]
    |   EscapeSequence
    ;

// §3.10.6 Escape Sequences for Character and String Literals

fragment
EscapeSequence
    :   '\\' [btnfr"'\\]
    ;

fragment
ZeroToThree
    :   [0-3]
    ;

// §3.10.7 The Null Literal

NullLiteral
    :   'null'
    ;

// §3.11 Separators

LPAREN          : '(';
RPAREN          : ')';
LBRACE          : '{';
RBRACE          : '}';
LBRACK          : '[';
RBRACK          : ']';
SEMI            : ';';
COMMA           : ',';
DOT             : '.';

// §3.12 Operators

ASSIGN          : '=';
GT              : '>';
LT              : '<';
BANG            : '!';
TILDE           : '~';
QUESTION        : '?';
COLON           : ':';
EQUAL           : '==';
LE              : '<=';
GE              : '>=';
NOTEQUAL        : '!=';
AND             : '&&';
OR              : '||';
INC             : '++';
DEC             : '--';
ADD             : '+';
SUB             : '-';
MUL             : '*';
DIV             : '/';
BITAND          : '&';
BITOR           : '|';
CARET           : '^';
MOD             : '%';
LSHIFT          : '<<';
RSHIFT          : '>>';

ADD_ASSIGN      : '+=';
SUB_ASSIGN      : '-=';
MUL_ASSIGN      : '*=';
DIV_ASSIGN      : '/=';
AND_ASSIGN      : '&=';
OR_ASSIGN       : '|=';
XOR_ASSIGN      : '^=';
MOD_ASSIGN      : '%=';
LSHIFT_ASSIGN   : '<<=';
RSHIFT_ASSIGN   : '>>=';
URSHIFT_ASSIGN  : '>>>=';

// §3.8 Identifiers (must appear after all keywords in the grammar)

Identifier
    :   JavaLetter JavaLetterOrDigit*
    ;

fragment
JavaLetter
    :   [a-zA-Z$_] // these are the "java letters" below 0x7F
    |   // covers all characters above 0x7F which are not a surrogate
        ~[\u0000-\u007F\uD800-\uDBFF]
        {Character.isJavaIdentifierStart(_input.LA(-1))}?
    |   // covers UTF-16 surrogate pairs encodings for U+10000 to U+10FFFF
        [\uD800-\uDBFF] [\uDC00-\uDFFF]
        {Character.isJavaIdentifierStart(Character.toCodePoint((char)_input.LA(-2), (char)_input.LA(-1)))}?
    ;

fragment
JavaLetterOrDigit
    :   [a-zA-Z0-9$_] // these are the "java letters or digits" below 0x7F
    |   // covers all characters above 0x7F which are not a surrogate
        ~[\u0000-\u007F\uD800-\uDBFF]
        {Character.isJavaIdentifierPart(_input.LA(-1))}?
    |   // covers UTF-16 surrogate pairs encodings for U+10000 to U+10FFFF
        [\uD800-\uDBFF] [\uDC00-\uDFFF]
        {Character.isJavaIdentifierPart(Character.toCodePoint((char)_input.LA(-2), (char)_input.LA(-1)))}?
    ;


//
// Whitespace and comments
//

WS  :  [ \t\r\n\u000C]+ -> skip
    ;

COMMENT
    :   '/*' .*? '*/' -> skip
    ;

LINE_COMMENT
    :   '//' ~[\r\n]* -> skip
    ;
