package parser;

// Generated from Mircnos.g4 by ANTLR 4.5.3
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class MircnosLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.5.3", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, BOOL=3, BREAK=4, CLASS=5, CONTINUE=6, ELSE=7, FOR=8, IF=9, 
		INT=10, NEW=11, RETURN=12, VOID=13, NULL=14, STRING=15, WHILE=16, IntegerLiteral=17, 
		BooleanLiteral=18, TRUE=19, FALSE=20, StringLiteral=21, NullLiteral=22, 
		LPAREN=23, RPAREN=24, LBRACE=25, RBRACE=26, LBRACK=27, RBRACK=28, SEMI=29, 
		COMMA=30, DOT=31, ASSIGN=32, GT=33, LT=34, BANG=35, TILDE=36, QUESTION=37, 
		COLON=38, EQUAL=39, LE=40, GE=41, NOTEQUAL=42, AND=43, OR=44, INC=45, 
		DEC=46, ADD=47, SUB=48, MUL=49, DIV=50, BITAND=51, BITOR=52, CARET=53, 
		MOD=54, LSHIFT=55, RSHIFT=56, ADD_ASSIGN=57, SUB_ASSIGN=58, MUL_ASSIGN=59, 
		DIV_ASSIGN=60, AND_ASSIGN=61, OR_ASSIGN=62, XOR_ASSIGN=63, MOD_ASSIGN=64, 
		LSHIFT_ASSIGN=65, RSHIFT_ASSIGN=66, URSHIFT_ASSIGN=67, Identifier=68, 
		WS=69, COMMENT=70, LINE_COMMENT=71;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"T__0", "T__1", "BOOL", "BREAK", "CLASS", "CONTINUE", "ELSE", "FOR", "IF", 
		"INT", "NEW", "RETURN", "VOID", "NULL", "STRING", "WHILE", "IntegerLiteral", 
		"DecimalIntegerLiteral", "IntegerTypeSuffix", "DecimalNumeral", "Digits", 
		"Digit", "NonZeroDigit", "DigitOrUnderscore", "Underscores", "BooleanLiteral", 
		"TRUE", "FALSE", "StringLiteral", "StringCharacters", "StringCharacter", 
		"EscapeSequence", "ZeroToThree", "NullLiteral", "LPAREN", "RPAREN", "LBRACE", 
		"RBRACE", "LBRACK", "RBRACK", "SEMI", "COMMA", "DOT", "ASSIGN", "GT", 
		"LT", "BANG", "TILDE", "QUESTION", "COLON", "EQUAL", "LE", "GE", "NOTEQUAL", 
		"AND", "OR", "INC", "DEC", "ADD", "SUB", "MUL", "DIV", "BITAND", "BITOR", 
		"CARET", "MOD", "LSHIFT", "RSHIFT", "ADD_ASSIGN", "SUB_ASSIGN", "MUL_ASSIGN", 
		"DIV_ASSIGN", "AND_ASSIGN", "OR_ASSIGN", "XOR_ASSIGN", "MOD_ASSIGN", "LSHIFT_ASSIGN", 
		"RSHIFT_ASSIGN", "URSHIFT_ASSIGN", "Identifier", "JavaLetter", "JavaLetterOrDigit", 
		"WS", "COMMENT", "LINE_COMMENT"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'[]'", "'this'", "'bool'", "'break'", "'class'", "'continue'", 
		"'else'", "'for'", "'if'", "'int'", "'new'", "'return'", "'void'", null, 
		"'string'", "'while'", null, null, "'true'", "'false'", null, null, "'('", 
		"')'", "'{'", "'}'", "'['", "']'", "';'", "','", "'.'", "'='", "'>'", 
		"'<'", "'!'", "'~'", "'?'", "':'", "'=='", "'<='", "'>='", "'!='", "'&&'", 
		"'||'", "'++'", "'--'", "'+'", "'-'", "'*'", "'/'", "'&'", "'|'", "'^'", 
		"'%'", "'<<'", "'>>'", "'+='", "'-='", "'*='", "'/='", "'&='", "'|='", 
		"'^='", "'%='", "'<<='", "'>>='", "'>>>='"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, "BOOL", "BREAK", "CLASS", "CONTINUE", "ELSE", "FOR", 
		"IF", "INT", "NEW", "RETURN", "VOID", "NULL", "STRING", "WHILE", "IntegerLiteral", 
		"BooleanLiteral", "TRUE", "FALSE", "StringLiteral", "NullLiteral", "LPAREN", 
		"RPAREN", "LBRACE", "RBRACE", "LBRACK", "RBRACK", "SEMI", "COMMA", "DOT", 
		"ASSIGN", "GT", "LT", "BANG", "TILDE", "QUESTION", "COLON", "EQUAL", "LE", 
		"GE", "NOTEQUAL", "AND", "OR", "INC", "DEC", "ADD", "SUB", "MUL", "DIV", 
		"BITAND", "BITOR", "CARET", "MOD", "LSHIFT", "RSHIFT", "ADD_ASSIGN", "SUB_ASSIGN", 
		"MUL_ASSIGN", "DIV_ASSIGN", "AND_ASSIGN", "OR_ASSIGN", "XOR_ASSIGN", "MOD_ASSIGN", 
		"LSHIFT_ASSIGN", "RSHIFT_ASSIGN", "URSHIFT_ASSIGN", "Identifier", "WS", 
		"COMMENT", "LINE_COMMENT"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public MircnosLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "Mircnos.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	@Override
	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 80:
			return JavaLetter_sempred((RuleContext)_localctx, predIndex);
		case 81:
			return JavaLetterOrDigit_sempred((RuleContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean JavaLetter_sempred(RuleContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return Character.isJavaIdentifierStart(_input.LA(-1));
		case 1:
			return Character.isJavaIdentifierStart(Character.toCodePoint((char)_input.LA(-2), (char)_input.LA(-1)));
		}
		return true;
	}
	private boolean JavaLetterOrDigit_sempred(RuleContext _localctx, int predIndex) {
		switch (predIndex) {
		case 2:
			return Character.isJavaIdentifierPart(_input.LA(-1));
		case 3:
			return Character.isJavaIdentifierPart(Character.toCodePoint((char)_input.LA(-2), (char)_input.LA(-1)));
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2I\u0207\b\1\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t"+
		"\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t;\4<\t<\4=\t="+
		"\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4C\tC\4D\tD\4E\tE\4F\tF\4G\tG\4H\tH\4I"+
		"\tI\4J\tJ\4K\tK\4L\tL\4M\tM\4N\tN\4O\tO\4P\tP\4Q\tQ\4R\tR\4S\tS\4T\tT"+
		"\4U\tU\4V\tV\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\5\3"+
		"\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7"+
		"\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\13\3\13\3\13"+
		"\3\13\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\16\3\16\3\16\3\16"+
		"\3\16\3\17\3\17\3\17\3\17\3\17\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\21"+
		"\3\21\3\21\3\21\3\21\3\21\3\22\3\22\3\23\3\23\5\23\u0106\n\23\3\24\3\24"+
		"\3\25\3\25\3\25\5\25\u010d\n\25\3\25\3\25\3\25\5\25\u0112\n\25\5\25\u0114"+
		"\n\25\3\26\3\26\7\26\u0118\n\26\f\26\16\26\u011b\13\26\3\26\5\26\u011e"+
		"\n\26\3\27\3\27\5\27\u0122\n\27\3\30\3\30\3\31\3\31\5\31\u0128\n\31\3"+
		"\32\6\32\u012b\n\32\r\32\16\32\u012c\3\33\3\33\3\33\3\33\3\33\3\33\3\33"+
		"\3\33\3\33\5\33\u0138\n\33\3\34\3\34\3\34\3\34\3\34\3\35\3\35\3\35\3\35"+
		"\3\35\3\35\3\36\3\36\5\36\u0147\n\36\3\36\3\36\3\37\6\37\u014c\n\37\r"+
		"\37\16\37\u014d\3 \3 \5 \u0152\n \3!\3!\3!\3\"\3\"\3#\3#\3#\3#\3#\3$\3"+
		"$\3%\3%\3&\3&\3\'\3\'\3(\3(\3)\3)\3*\3*\3+\3+\3,\3,\3-\3-\3.\3.\3/\3/"+
		"\3\60\3\60\3\61\3\61\3\62\3\62\3\63\3\63\3\64\3\64\3\64\3\65\3\65\3\65"+
		"\3\66\3\66\3\66\3\67\3\67\3\67\38\38\38\39\39\39\3:\3:\3:\3;\3;\3;\3<"+
		"\3<\3=\3=\3>\3>\3?\3?\3@\3@\3A\3A\3B\3B\3C\3C\3D\3D\3D\3E\3E\3E\3F\3F"+
		"\3F\3G\3G\3G\3H\3H\3H\3I\3I\3I\3J\3J\3J\3K\3K\3K\3L\3L\3L\3M\3M\3M\3N"+
		"\3N\3N\3N\3O\3O\3O\3O\3P\3P\3P\3P\3P\3Q\3Q\7Q\u01d3\nQ\fQ\16Q\u01d6\13"+
		"Q\3R\3R\3R\3R\3R\3R\5R\u01de\nR\3S\3S\3S\3S\3S\3S\5S\u01e6\nS\3T\6T\u01e9"+
		"\nT\rT\16T\u01ea\3T\3T\3U\3U\3U\3U\7U\u01f3\nU\fU\16U\u01f6\13U\3U\3U"+
		"\3U\3U\3U\3V\3V\3V\3V\7V\u0201\nV\fV\16V\u0204\13V\3V\3V\3\u01f4\2W\3"+
		"\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20\37"+
		"\21!\22#\23%\2\'\2)\2+\2-\2/\2\61\2\63\2\65\24\67\259\26;\27=\2?\2A\2"+
		"C\2E\30G\31I\32K\33M\34O\35Q\36S\37U W!Y\"[#]$_%a&c\'e(g)i*k+m,o-q.s/"+
		"u\60w\61y\62{\63}\64\177\65\u0081\66\u0083\67\u00858\u00879\u0089:\u008b"+
		";\u008d<\u008f=\u0091>\u0093?\u0095@\u0097A\u0099B\u009bC\u009dD\u009f"+
		"E\u00a1F\u00a3\2\u00a5\2\u00a7G\u00a9H\u00abI\3\2\16\4\2NNnn\3\2\63;\4"+
		"\2$$^^\n\2$$))^^ddhhppttvv\3\2\62\65\6\2&&C\\aac|\4\2\2\u0081\ud802\udc01"+
		"\3\2\ud802\udc01\3\2\udc02\ue001\7\2&&\62;C\\aac|\5\2\13\f\16\17\"\"\4"+
		"\2\f\f\17\17\u020d\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13"+
		"\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2"+
		"\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2"+
		"!\3\2\2\2\2#\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3\2\2\2\2"+
		"E\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2\2K\3\2\2\2\2M\3\2\2\2\2O\3\2\2\2\2Q\3"+
		"\2\2\2\2S\3\2\2\2\2U\3\2\2\2\2W\3\2\2\2\2Y\3\2\2\2\2[\3\2\2\2\2]\3\2\2"+
		"\2\2_\3\2\2\2\2a\3\2\2\2\2c\3\2\2\2\2e\3\2\2\2\2g\3\2\2\2\2i\3\2\2\2\2"+
		"k\3\2\2\2\2m\3\2\2\2\2o\3\2\2\2\2q\3\2\2\2\2s\3\2\2\2\2u\3\2\2\2\2w\3"+
		"\2\2\2\2y\3\2\2\2\2{\3\2\2\2\2}\3\2\2\2\2\177\3\2\2\2\2\u0081\3\2\2\2"+
		"\2\u0083\3\2\2\2\2\u0085\3\2\2\2\2\u0087\3\2\2\2\2\u0089\3\2\2\2\2\u008b"+
		"\3\2\2\2\2\u008d\3\2\2\2\2\u008f\3\2\2\2\2\u0091\3\2\2\2\2\u0093\3\2\2"+
		"\2\2\u0095\3\2\2\2\2\u0097\3\2\2\2\2\u0099\3\2\2\2\2\u009b\3\2\2\2\2\u009d"+
		"\3\2\2\2\2\u009f\3\2\2\2\2\u00a1\3\2\2\2\2\u00a7\3\2\2\2\2\u00a9\3\2\2"+
		"\2\2\u00ab\3\2\2\2\3\u00ad\3\2\2\2\5\u00b0\3\2\2\2\7\u00b5\3\2\2\2\t\u00ba"+
		"\3\2\2\2\13\u00c0\3\2\2\2\r\u00c6\3\2\2\2\17\u00cf\3\2\2\2\21\u00d4\3"+
		"\2\2\2\23\u00d8\3\2\2\2\25\u00db\3\2\2\2\27\u00df\3\2\2\2\31\u00e3\3\2"+
		"\2\2\33\u00ea\3\2\2\2\35\u00ef\3\2\2\2\37\u00f4\3\2\2\2!\u00fb\3\2\2\2"+
		"#\u0101\3\2\2\2%\u0103\3\2\2\2\'\u0107\3\2\2\2)\u0113\3\2\2\2+\u0115\3"+
		"\2\2\2-\u0121\3\2\2\2/\u0123\3\2\2\2\61\u0127\3\2\2\2\63\u012a\3\2\2\2"+
		"\65\u0137\3\2\2\2\67\u0139\3\2\2\29\u013e\3\2\2\2;\u0144\3\2\2\2=\u014b"+
		"\3\2\2\2?\u0151\3\2\2\2A\u0153\3\2\2\2C\u0156\3\2\2\2E\u0158\3\2\2\2G"+
		"\u015d\3\2\2\2I\u015f\3\2\2\2K\u0161\3\2\2\2M\u0163\3\2\2\2O\u0165\3\2"+
		"\2\2Q\u0167\3\2\2\2S\u0169\3\2\2\2U\u016b\3\2\2\2W\u016d\3\2\2\2Y\u016f"+
		"\3\2\2\2[\u0171\3\2\2\2]\u0173\3\2\2\2_\u0175\3\2\2\2a\u0177\3\2\2\2c"+
		"\u0179\3\2\2\2e\u017b\3\2\2\2g\u017d\3\2\2\2i\u0180\3\2\2\2k\u0183\3\2"+
		"\2\2m\u0186\3\2\2\2o\u0189\3\2\2\2q\u018c\3\2\2\2s\u018f\3\2\2\2u\u0192"+
		"\3\2\2\2w\u0195\3\2\2\2y\u0197\3\2\2\2{\u0199\3\2\2\2}\u019b\3\2\2\2\177"+
		"\u019d\3\2\2\2\u0081\u019f\3\2\2\2\u0083\u01a1\3\2\2\2\u0085\u01a3\3\2"+
		"\2\2\u0087\u01a5\3\2\2\2\u0089\u01a8\3\2\2\2\u008b\u01ab\3\2\2\2\u008d"+
		"\u01ae\3\2\2\2\u008f\u01b1\3\2\2\2\u0091\u01b4\3\2\2\2\u0093\u01b7\3\2"+
		"\2\2\u0095\u01ba\3\2\2\2\u0097\u01bd\3\2\2\2\u0099\u01c0\3\2\2\2\u009b"+
		"\u01c3\3\2\2\2\u009d\u01c7\3\2\2\2\u009f\u01cb\3\2\2\2\u00a1\u01d0\3\2"+
		"\2\2\u00a3\u01dd\3\2\2\2\u00a5\u01e5\3\2\2\2\u00a7\u01e8\3\2\2\2\u00a9"+
		"\u01ee\3\2\2\2\u00ab\u01fc\3\2\2\2\u00ad\u00ae\7]\2\2\u00ae\u00af\7_\2"+
		"\2\u00af\4\3\2\2\2\u00b0\u00b1\7v\2\2\u00b1\u00b2\7j\2\2\u00b2\u00b3\7"+
		"k\2\2\u00b3\u00b4\7u\2\2\u00b4\6\3\2\2\2\u00b5\u00b6\7d\2\2\u00b6\u00b7"+
		"\7q\2\2\u00b7\u00b8\7q\2\2\u00b8\u00b9\7n\2\2\u00b9\b\3\2\2\2\u00ba\u00bb"+
		"\7d\2\2\u00bb\u00bc\7t\2\2\u00bc\u00bd\7g\2\2\u00bd\u00be\7c\2\2\u00be"+
		"\u00bf\7m\2\2\u00bf\n\3\2\2\2\u00c0\u00c1\7e\2\2\u00c1\u00c2\7n\2\2\u00c2"+
		"\u00c3\7c\2\2\u00c3\u00c4\7u\2\2\u00c4\u00c5\7u\2\2\u00c5\f\3\2\2\2\u00c6"+
		"\u00c7\7e\2\2\u00c7\u00c8\7q\2\2\u00c8\u00c9\7p\2\2\u00c9\u00ca\7v\2\2"+
		"\u00ca\u00cb\7k\2\2\u00cb\u00cc\7p\2\2\u00cc\u00cd\7w\2\2\u00cd\u00ce"+
		"\7g\2\2\u00ce\16\3\2\2\2\u00cf\u00d0\7g\2\2\u00d0\u00d1\7n\2\2\u00d1\u00d2"+
		"\7u\2\2\u00d2\u00d3\7g\2\2\u00d3\20\3\2\2\2\u00d4\u00d5\7h\2\2\u00d5\u00d6"+
		"\7q\2\2\u00d6\u00d7\7t\2\2\u00d7\22\3\2\2\2\u00d8\u00d9\7k\2\2\u00d9\u00da"+
		"\7h\2\2\u00da\24\3\2\2\2\u00db\u00dc\7k\2\2\u00dc\u00dd\7p\2\2\u00dd\u00de"+
		"\7v\2\2\u00de\26\3\2\2\2\u00df\u00e0\7p\2\2\u00e0\u00e1\7g\2\2\u00e1\u00e2"+
		"\7y\2\2\u00e2\30\3\2\2\2\u00e3\u00e4\7t\2\2\u00e4\u00e5\7g\2\2\u00e5\u00e6"+
		"\7v\2\2\u00e6\u00e7\7w\2\2\u00e7\u00e8\7t\2\2\u00e8\u00e9\7p\2\2\u00e9"+
		"\32\3\2\2\2\u00ea\u00eb\7x\2\2\u00eb\u00ec\7q\2\2\u00ec\u00ed\7k\2\2\u00ed"+
		"\u00ee\7f\2\2\u00ee\34\3\2\2\2\u00ef\u00f0\7p\2\2\u00f0\u00f1\7w\2\2\u00f1"+
		"\u00f2\7n\2\2\u00f2\u00f3\7n\2\2\u00f3\36\3\2\2\2\u00f4\u00f5\7u\2\2\u00f5"+
		"\u00f6\7v\2\2\u00f6\u00f7\7t\2\2\u00f7\u00f8\7k\2\2\u00f8\u00f9\7p\2\2"+
		"\u00f9\u00fa\7i\2\2\u00fa \3\2\2\2\u00fb\u00fc\7y\2\2\u00fc\u00fd\7j\2"+
		"\2\u00fd\u00fe\7k\2\2\u00fe\u00ff\7n\2\2\u00ff\u0100\7g\2\2\u0100\"\3"+
		"\2\2\2\u0101\u0102\5%\23\2\u0102$\3\2\2\2\u0103\u0105\5)\25\2\u0104\u0106"+
		"\5\'\24\2\u0105\u0104\3\2\2\2\u0105\u0106\3\2\2\2\u0106&\3\2\2\2\u0107"+
		"\u0108\t\2\2\2\u0108(\3\2\2\2\u0109\u0114\7\62\2\2\u010a\u0111\5/\30\2"+
		"\u010b\u010d\5+\26\2\u010c\u010b\3\2\2\2\u010c\u010d\3\2\2\2\u010d\u0112"+
		"\3\2\2\2\u010e\u010f\5\63\32\2\u010f\u0110\5+\26\2\u0110\u0112\3\2\2\2"+
		"\u0111\u010c\3\2\2\2\u0111\u010e\3\2\2\2\u0112\u0114\3\2\2\2\u0113\u0109"+
		"\3\2\2\2\u0113\u010a\3\2\2\2\u0114*\3\2\2\2\u0115\u011d\5-\27\2\u0116"+
		"\u0118\5\61\31\2\u0117\u0116\3\2\2\2\u0118\u011b\3\2\2\2\u0119\u0117\3"+
		"\2\2\2\u0119\u011a\3\2\2\2\u011a\u011c\3\2\2\2\u011b\u0119\3\2\2\2\u011c"+
		"\u011e\5-\27\2\u011d\u0119\3\2\2\2\u011d\u011e\3\2\2\2\u011e,\3\2\2\2"+
		"\u011f\u0122\7\62\2\2\u0120\u0122\5/\30\2\u0121\u011f\3\2\2\2\u0121\u0120"+
		"\3\2\2\2\u0122.\3\2\2\2\u0123\u0124\t\3\2\2\u0124\60\3\2\2\2\u0125\u0128"+
		"\5-\27\2\u0126\u0128\7a\2\2\u0127\u0125\3\2\2\2\u0127\u0126\3\2\2\2\u0128"+
		"\62\3\2\2\2\u0129\u012b\7a\2\2\u012a\u0129\3\2\2\2\u012b\u012c\3\2\2\2"+
		"\u012c\u012a\3\2\2\2\u012c\u012d\3\2\2\2\u012d\64\3\2\2\2\u012e\u012f"+
		"\7v\2\2\u012f\u0130\7t\2\2\u0130\u0131\7w\2\2\u0131\u0138\7g\2\2\u0132"+
		"\u0133\7h\2\2\u0133\u0134\7c\2\2\u0134\u0135\7n\2\2\u0135\u0136\7u\2\2"+
		"\u0136\u0138\7g\2\2\u0137\u012e\3\2\2\2\u0137\u0132\3\2\2\2\u0138\66\3"+
		"\2\2\2\u0139\u013a\7v\2\2\u013a\u013b\7t\2\2\u013b\u013c\7w\2\2\u013c"+
		"\u013d\7g\2\2\u013d8\3\2\2\2\u013e\u013f\7h\2\2\u013f\u0140\7c\2\2\u0140"+
		"\u0141\7n\2\2\u0141\u0142\7u\2\2\u0142\u0143\7g\2\2\u0143:\3\2\2\2\u0144"+
		"\u0146\7$\2\2\u0145\u0147\5=\37\2\u0146\u0145\3\2\2\2\u0146\u0147\3\2"+
		"\2\2\u0147\u0148\3\2\2\2\u0148\u0149\7$\2\2\u0149<\3\2\2\2\u014a\u014c"+
		"\5? \2\u014b\u014a\3\2\2\2\u014c\u014d\3\2\2\2\u014d\u014b\3\2\2\2\u014d"+
		"\u014e\3\2\2\2\u014e>\3\2\2\2\u014f\u0152\n\4\2\2\u0150\u0152\5A!\2\u0151"+
		"\u014f\3\2\2\2\u0151\u0150\3\2\2\2\u0152@\3\2\2\2\u0153\u0154\7^\2\2\u0154"+
		"\u0155\t\5\2\2\u0155B\3\2\2\2\u0156\u0157\t\6\2\2\u0157D\3\2\2\2\u0158"+
		"\u0159\7p\2\2\u0159\u015a\7w\2\2\u015a\u015b\7n\2\2\u015b\u015c\7n\2\2"+
		"\u015cF\3\2\2\2\u015d\u015e\7*\2\2\u015eH\3\2\2\2\u015f\u0160\7+\2\2\u0160"+
		"J\3\2\2\2\u0161\u0162\7}\2\2\u0162L\3\2\2\2\u0163\u0164\7\177\2\2\u0164"+
		"N\3\2\2\2\u0165\u0166\7]\2\2\u0166P\3\2\2\2\u0167\u0168\7_\2\2\u0168R"+
		"\3\2\2\2\u0169\u016a\7=\2\2\u016aT\3\2\2\2\u016b\u016c\7.\2\2\u016cV\3"+
		"\2\2\2\u016d\u016e\7\60\2\2\u016eX\3\2\2\2\u016f\u0170\7?\2\2\u0170Z\3"+
		"\2\2\2\u0171\u0172\7@\2\2\u0172\\\3\2\2\2\u0173\u0174\7>\2\2\u0174^\3"+
		"\2\2\2\u0175\u0176\7#\2\2\u0176`\3\2\2\2\u0177\u0178\7\u0080\2\2\u0178"+
		"b\3\2\2\2\u0179\u017a\7A\2\2\u017ad\3\2\2\2\u017b\u017c\7<\2\2\u017cf"+
		"\3\2\2\2\u017d\u017e\7?\2\2\u017e\u017f\7?\2\2\u017fh\3\2\2\2\u0180\u0181"+
		"\7>\2\2\u0181\u0182\7?\2\2\u0182j\3\2\2\2\u0183\u0184\7@\2\2\u0184\u0185"+
		"\7?\2\2\u0185l\3\2\2\2\u0186\u0187\7#\2\2\u0187\u0188\7?\2\2\u0188n\3"+
		"\2\2\2\u0189\u018a\7(\2\2\u018a\u018b\7(\2\2\u018bp\3\2\2\2\u018c\u018d"+
		"\7~\2\2\u018d\u018e\7~\2\2\u018er\3\2\2\2\u018f\u0190\7-\2\2\u0190\u0191"+
		"\7-\2\2\u0191t\3\2\2\2\u0192\u0193\7/\2\2\u0193\u0194\7/\2\2\u0194v\3"+
		"\2\2\2\u0195\u0196\7-\2\2\u0196x\3\2\2\2\u0197\u0198\7/\2\2\u0198z\3\2"+
		"\2\2\u0199\u019a\7,\2\2\u019a|\3\2\2\2\u019b\u019c\7\61\2\2\u019c~\3\2"+
		"\2\2\u019d\u019e\7(\2\2\u019e\u0080\3\2\2\2\u019f\u01a0\7~\2\2\u01a0\u0082"+
		"\3\2\2\2\u01a1\u01a2\7`\2\2\u01a2\u0084\3\2\2\2\u01a3\u01a4\7\'\2\2\u01a4"+
		"\u0086\3\2\2\2\u01a5\u01a6\7>\2\2\u01a6\u01a7\7>\2\2\u01a7\u0088\3\2\2"+
		"\2\u01a8\u01a9\7@\2\2\u01a9\u01aa\7@\2\2\u01aa\u008a\3\2\2\2\u01ab\u01ac"+
		"\7-\2\2\u01ac\u01ad\7?\2\2\u01ad\u008c\3\2\2\2\u01ae\u01af\7/\2\2\u01af"+
		"\u01b0\7?\2\2\u01b0\u008e\3\2\2\2\u01b1\u01b2\7,\2\2\u01b2\u01b3\7?\2"+
		"\2\u01b3\u0090\3\2\2\2\u01b4\u01b5\7\61\2\2\u01b5\u01b6\7?\2\2\u01b6\u0092"+
		"\3\2\2\2\u01b7\u01b8\7(\2\2\u01b8\u01b9\7?\2\2\u01b9\u0094\3\2\2\2\u01ba"+
		"\u01bb\7~\2\2\u01bb\u01bc\7?\2\2\u01bc\u0096\3\2\2\2\u01bd\u01be\7`\2"+
		"\2\u01be\u01bf\7?\2\2\u01bf\u0098\3\2\2\2\u01c0\u01c1\7\'\2\2\u01c1\u01c2"+
		"\7?\2\2\u01c2\u009a\3\2\2\2\u01c3\u01c4\7>\2\2\u01c4\u01c5\7>\2\2\u01c5"+
		"\u01c6\7?\2\2\u01c6\u009c\3\2\2\2\u01c7\u01c8\7@\2\2\u01c8\u01c9\7@\2"+
		"\2\u01c9\u01ca\7?\2\2\u01ca\u009e\3\2\2\2\u01cb\u01cc\7@\2\2\u01cc\u01cd"+
		"\7@\2\2\u01cd\u01ce\7@\2\2\u01ce\u01cf\7?\2\2\u01cf\u00a0\3\2\2\2\u01d0"+
		"\u01d4\5\u00a3R\2\u01d1\u01d3\5\u00a5S\2\u01d2\u01d1\3\2\2\2\u01d3\u01d6"+
		"\3\2\2\2\u01d4\u01d2\3\2\2\2\u01d4\u01d5\3\2\2\2\u01d5\u00a2\3\2\2\2\u01d6"+
		"\u01d4\3\2\2\2\u01d7\u01de\t\7\2\2\u01d8\u01d9\n\b\2\2\u01d9\u01de\6R"+
		"\2\2\u01da\u01db\t\t\2\2\u01db\u01dc\t\n\2\2\u01dc\u01de\6R\3\2\u01dd"+
		"\u01d7\3\2\2\2\u01dd\u01d8\3\2\2\2\u01dd\u01da\3\2\2\2\u01de\u00a4\3\2"+
		"\2\2\u01df\u01e6\t\13\2\2\u01e0\u01e1\n\b\2\2\u01e1\u01e6\6S\4\2\u01e2"+
		"\u01e3\t\t\2\2\u01e3\u01e4\t\n\2\2\u01e4\u01e6\6S\5\2\u01e5\u01df\3\2"+
		"\2\2\u01e5\u01e0\3\2\2\2\u01e5\u01e2\3\2\2\2\u01e6\u00a6\3\2\2\2\u01e7"+
		"\u01e9\t\f\2\2\u01e8\u01e7\3\2\2\2\u01e9\u01ea\3\2\2\2\u01ea\u01e8\3\2"+
		"\2\2\u01ea\u01eb\3\2\2\2\u01eb\u01ec\3\2\2\2\u01ec\u01ed\bT\2\2\u01ed"+
		"\u00a8\3\2\2\2\u01ee\u01ef\7\61\2\2\u01ef\u01f0\7,\2\2\u01f0\u01f4\3\2"+
		"\2\2\u01f1\u01f3\13\2\2\2\u01f2\u01f1\3\2\2\2\u01f3\u01f6\3\2\2\2\u01f4"+
		"\u01f5\3\2\2\2\u01f4\u01f2\3\2\2\2\u01f5\u01f7\3\2\2\2\u01f6\u01f4\3\2"+
		"\2\2\u01f7\u01f8\7,\2\2\u01f8\u01f9\7\61\2\2\u01f9\u01fa\3\2\2\2\u01fa"+
		"\u01fb\bU\2\2\u01fb\u00aa\3\2\2\2\u01fc\u01fd\7\61\2\2\u01fd\u01fe\7\61"+
		"\2\2\u01fe\u0202\3\2\2\2\u01ff\u0201\n\r\2\2\u0200\u01ff\3\2\2\2\u0201"+
		"\u0204\3\2\2\2\u0202\u0200\3\2\2\2\u0202\u0203\3\2\2\2\u0203\u0205\3\2"+
		"\2\2\u0204\u0202\3\2\2\2\u0205\u0206\bV\2\2\u0206\u00ac\3\2\2\2\26\2\u0105"+
		"\u010c\u0111\u0113\u0119\u011d\u0121\u0127\u012c\u0137\u0146\u014d\u0151"+
		"\u01d4\u01dd\u01e5\u01ea\u01f4\u0202\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}